# Default values for tuf.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.
#
#
image:
  pullPolicy: IfNotPresent
  repository: registry.gitlab.com/drupal-infrastructure/package-signing/drupal-rugged
  # Overrides the image tag whose default is the chart appVersion.
  tag: "2025-02-07-01"

timestamp:
  replicaCount: 1
  env:
    - name: RUGGED_WORKER
      value: "timestamp-worker"
  resources:
    requests:
      memory: 200Mi
  podAnnotations: {}

snapshot:
  env:
    - name: RUGGED_WORKER
      value: "snapshot-worker"
  resources:
    requests:
      memory: 1.5Gi
  podAnnotations: {}

targets:
  replicaCount: 1
  env:
    - name: RUGGED_WORKER
      value: "targets-worker"
  resources:
    requests:
      memory: 1.5Gi
  podAnnotations: {}

monitor:
  replicaCount: 1
  env:
    - name: RUGGED_WORKER
      value: "monitor-worker"
  resources:
    requests:
      memory: 200Mi
  podAnnotations: {}

root:
  replicaCount: 1
  env:
    - name: RUGGED_WORKER
      value: "root-worker"
  resources: {}
  podAnnotations: {}

# Serves tuf metatdata with nginx if enabled.
nginx:
  enabled: true
  replicaCount: 1
  image:
    repository: registry.hub.docker.com/bitnami/nginx
    pullPolicy: IfNotPresent
    tag: "1.24"
  config:
    # Server root, tuf_repo/metadata mounted here.
    # ( Templated into server block if not using custom config override )
    serverRoot: /app

    # Mount templated server block to be included in nginx config.
    serverBlock:
      # Where to mount server block config such that it is included in nginx conf.
      mountPath: /opt/bitnami/nginx/conf/server_blocks/
      fileName: default-server.conf
      # Include inline in server block for quick customization.
      serverIncludeInline: |-
        autoindex on; # enable directory listing, for example.

  # Nginx container's listen port, templated into optional server block.
  containerPort: 8080
  # Overrides global 'securityContext' values
  securityContext:
    # bitnami/nginx runs as 1001
    runAsUser: 1001
  env: {}
  resources: {}

# Enable metrics sidecar on monitoring worker
metrics:
  enabled: false
  port: 8080
  # How often metrics are updated
  collectionPeriod: 30
  # log verbosity (can be increased by setting to `debug` or `trace`).
  logVerbosity: info
  # Set to true to enable datadog openmetrics auto-discovery annotation
  includeDatadogAnnotation: false

  image:
    repository: registry.gitlab.com/drupal-infrastructure/package-signing/rugged-metrics
    pullPolicy: IfNotPresent
    tag: "0.2.0"
  securityContext:
    capabilities:
      drop:
      - ALL
    runAsNonRoot: true
    privileged: false

imagePullSecrets:
  - name: gitlab-pull-secret

nameOverride: ""
fullnameOverride: ""

config:
  rabbitmq:
    host: tuf-rabbitmq
    username: rabbitmq
    password: badpassword
    # Set to amqps for TLS
    protocol: pyamqp

  # "This should be a power of 2, as this will allow even distribution of hash prefixes across all bins."
  number_of_bins: 1024

  # comment from rugged/default_config.yaml:
  #
  # To set a different key to be used by hashed bins roles, you will also need to
  # specify the name as a key in both the 'keys' and 'roles' arrays. For an
  # example, see: features/fixtures/config/hashed_bins_with_key.yaml
  hashed_bins_key_name: targets

commonConfig:
  roles:
    root:
      threshold: 1
      expiry: 31536000 # 365 days
    timestamp:
      threshold: 1
      expiry: 86400    # 1 day
    snapshot:
      threshold: 1
      expiry: 604800   # 7 days
    targets:
      threshold: 1
      expiry: 604800   # 7 days

  stale_semaphore_age_thresholds:
    tuf_paused: 3600            # 1 hour (set manually, no automatic cleanup)
    tuf_processing_: 300        # 5 minutes
    tuf_refreshing_expiry: 60   # 1 minute
  
  # Max memory a child worker can allocate before being replaced (unit: kilobytes).
  # Setting this limits growth of celery workers over time, if task uses more than limit,
  # will complete before replacing worker.
  # (https://docs.celeryq.dev/en/latest/userguide/configuration.html#worker-max-memory-per-child)
  celery_worker_max_memory_per_child: 3000000 #3gb (unit kb)


monitorConfig:
  workers:
    snapshot:
      name: snapshot-worker
    targets:
      name: targets-worker
    timestamp:
      name: timestamp-worker
  scheduler_log_level: INFO
  scheduler_scan_period: 5.0
  scheduler_refresh_period: 0
  scheduler_reset_period: 300.0
  task_timeout: 600

rootConfig:
  workers:
    snapshot:
      name: snapshot-worker
    targets:
      name: targets-worker
    timestamp:
      name: timestamp-worker
    root:
      name: root-worker
    monitor:
      name: monitor-worker
  keys:
    root:
      - root
      - root1
    snapshot:
      - snapshot
    targets:
      - targets
    timestamp:
      - timestamp
  scheduler_log_level: INFO
  scheduler_scan_period: 5.0
  use_hashed_bins: true
  task_timeout: 600

snapshotConfig:
  use_hashed_bins: true

targetsConfig:
  use_hashed_bins: true
  delete_targets_after_signing: True

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}
serviceAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

storage:
  #storageClassName:
  create: true
  size: 500Mi
  accessModes:
    - ReadWriteMany


# Default security context
securityContext:
  #readOnlyRootFilesystem: true
  capabilities:
    drop:
    - ALL
  runAsNonRoot: true
  privileged: false
  runAsUser: 440
  runAsGroup: 440

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  className: ""
  annotations:
    kubernetes.io/ingress.class: traefik
    kubernetes.io/tls-acme: "true"
    traefik.ingress.kubernetes.io/router.entrypoints: websecure
    traefik.ingress.kubernetes.io/router.tls: "true"
  hosts:
    - host: chart-example.local
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local


autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}

rabbitmq:
  enabled: true
  auth:
    username: rabbitmq
    password: badpassword


sftp-server:
  service:
    enabled: true
    type: ClusterIP
    port: 22
  sftpConfig:
    username: rugged
    # Password omitted to require authorization via ssh key
    # password: badpassword

    # is specified password "encrypted" (i.e. provided as hash)
    encrypted: false
    uid: "440"
    gid: "440"
    # Secret should be created with listed entries for host keys.
    # If omitted, will generate keys during init and have inconsistent fingerprint.
    # hostKeys:
    #   secret: sftp-host-keys
    #   keys:
    #   - ssh_host_ed25519_key
    #   - ssh_host_rsa_key

    # Name of secret containing key/val pairs of authorized public keys
    # authorizedKeysSecret: "sftp-authorized-keys"

  # volumeMounts:
  # - name: "tuf-targets"
  #   claimName: "tuf-targets"
  #   relativeMountPath: "incoming_targets/"
  #   # subPath: ""
