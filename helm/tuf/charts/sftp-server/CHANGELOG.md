# Changelog

* Refactored templating around pvc mounts, `volumeMounts` array now allows specifying multiple PVCs and specifying relative mountpath to sftp session chroot.
* Removed vxlancontroller + debug chart features
* Removed NET_ADMIN capability.
* Relaxed liveness probe to prevent premature restart of pod.
* Store authorized keys in secret.

** Chart has been effectively forked from openvnf/sftp-server, changes noted above **

### 0.3.2

* add .helmignore for a clean chart

### 0.3.1

* Move debug container to first position to make it the default target for container attachments.
* Fix inclusion of resource definitions.

### 0.3.0

* Added `NET_ADMIN` capabilities to the sftp-server container.
* Added nettools-debug containers (disabled by default)
* Changed sftp resource-definition from .resources to resources.sftp

### 0.2.0

* change update strategy to `Recreate` to prevent deadlock on PV allocation

### 0.1.1

* change image repository to `quay.io/openvnf/sftp`
