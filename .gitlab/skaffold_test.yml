rugged-skaffold-test:
  stage: test
  image: docker:20.10.16
  variables:
    # connect to docker in docker service with TLS
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_TLS_VERIFY: "1"
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
    KIND_NODE_VERSION: v1.22.17
    SKAFFOLD_ENV: CI

  services:
    # docker in docker to host kind cluster
  - name: docker:20.10.16-dind
    # If we access api server with hostname 'docker', this hostname is not present
    # in cluster's certificate (unless we add it). Alias service for simple fix.
    alias: kubernetes
  before_script:
    # Install kubectl
    - wget "https://dl.k8s.io/release/v1.27.1/bin/linux/amd64/kubectl"
    - chmod +x kubectl
    - mv kubectl /usr/local/bin
    # Install Kind
    - wget "https://kind.sigs.k8s.io/dl/v0.18.0/kind-linux-amd64"
    - mv ./kind-linux-amd64 /usr/local/bin/kind
    - chmod ugo+x /usr/local/bin/kind
    # Install skaffold
    - wget "https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64"
    - mv ./skaffold-linux-amd64 ./skaffold
    - install skaffold /usr/local/bin/
    # Install helm
    - wget https://get.helm.sh/helm-v3.12.0-linux-amd64.tar.gz
    - tar -xzf helm-v3.12.0-linux-amd64.tar.gz
    - mv linux-amd64/helm /usr/local/bin
    # Configure Kind cluster
    - |
      echo -e "
      kind: Cluster
      apiVersion: kind.x-k8s.io/v1alpha4
      networking:
        # Allow connection to api server outside of dind container.
        apiServerAddress: 0.0.0.0
      " > config.yaml
    - kind create cluster --name cluster --image kindest/node:$KIND_NODE_VERSION --wait 180s --config config.yaml
    # Update kubeconfig to connect to dind container hosting kind node.
    - sed -i -E -e s/0\.0\.0\.0/kubernetes/g "$HOME/.kube/config"
    # Add helm repos for chart subdependencies
    - helm repo add bitnami https://charts.bitnami.com/bitnami
  script:
    # Use tag "latest" if running on ci default branch (drupal-main), else use tag branch-<branch name>
    - |
      echo running on branch: $CI_COMMIT_REF_NAME;
      if [ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ]
      then
        IMAGE_TAG="latest";
      elif [ -n "$CI_COMMIT_TAG" ]
      then
        IMAGE_TAG="$CI_COMMIT_TAG"
      else
        IMAGE_TAG="branch-$CI_COMMIT_REF_NAME";
      fi
      echo Using image tag: $IMAGE_TAG;
    - REGISTRY_PATH=$(echo "$CI_PROJECT_NAMESPACE" | awk '{print tolower($0)}')
    # overrides skaffold registry from local to gitlab project registry
    - skaffold deploy --default-repo $CI_REGISTRY/$REGISTRY_PATH --tag $IMAGE_TAG
    # Smoke test rugged commands for errors.
    # (Will not fail on invalid behaviour, only failed commands, TOOD: validate output?)
    - kubectl get pods -o wide;
    - ROOT_POD_NAME=$(kubectl get pod --selector rugged-worker=root --no-headers -o custom-columns=":metadata.name");
    # Wait 30 seconds to make sure rabbitMQ is fully booted.
    - sleep 30;
    - kubectl exec $ROOT_POD_NAME -- sh -c 'rugged generate-keys';
    - kubectl exec $ROOT_POD_NAME -- sh -c 'rugged initialize';
    - kubectl exec $ROOT_POD_NAME -- sh -c 'rugged status';
    - kubectl exec $ROOT_POD_NAME -- sh -c 'rugged logs'
    # Test nginx service is serving metadata
    - NGINX_CLUSTER_IP=$(kubectl get service -l app.kubernetes.io/name=tuf --no-headers -o custom-columns=:spec.clusterIP)
    - "echo Curling nginx serve: $NGINX_CLUSTER_IP:80/metadata/1.root.json from temporary pod..."
    - kubectl run nginx-metadata-test --image curlimages/curl -it --rm --restart=Never -- curl --fail-with-body $NGINX_CLUSTER_IP:80/metadata/1.root.json
  after_script:
    # dump logs of workers.
    - kubectl get pods -o wide;
    - kubectl logs -l app.kubernetes.io/name=tuf;
    # log rabbitMQ
    - kubectl logs local-rabbitmq-0;
    # cleanup kind cluster
    - kind delete cluster --name cluster
