---
title: Privacy Policy 
menuTitle: Privacy
weight: 100
---

At https://rugged.works, we respect your privacy. This website is a static site dedicated to providing documentation for the Rugged TUF Server. We do not collect, store, or process any personal data from visitors to this site.

Data Collection
We do not collect any personal information, such as names, email addresses, or IP addresses, when you visit our site.

Cookies
This website does not use cookies to track user activity or store any personal information.

Third-Party Services
This website does not integrate with any third-party services that collect user data.

By using this website, you agree to our Privacy Policy. If you have any questions or concerns, feel free to contact [Consensus Enterprises].

[Consensus Enterprises]: mailto:info@consensus.enterprises
