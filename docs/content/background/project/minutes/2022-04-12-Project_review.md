---
title: Project Review & Planning - 2022-04-12
menutitle: 2022-04-12 Project Review
hidden: true
date: 2022-04-12

---

Project docs: https://drupal-infrastructure.gitlab.io/package-signing/tuf
Etherpad: https://pad.riseup.net/p/da-tuf-project-review

## Check-in (5m)

(we don't usually take notes for this section)

## Admin (5m)

A - Attendance

* Invitees: Tim, Christopher
* Attendees: Tim, Christopher
* Regrets: None

D - Duration (55m)

M - Minutes: https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-04-08-sprint/

I - Information
* Christopher cloned the project to https://gitlab.com/rugged/rugged (also see: https://rugged.works), cleaned up the tickets to remove DA project-specifics. Docs still need to be split up.

N - Next Meeting (ad hoc)
* TBD (if req'd)


## Agenda

### Review open tickets

* [x] `rugged/rugged` now has "technical" tickets: https://gitlab.com/rugged/rugged/-/boards
* [x] Some tickets may no longer be relevant: https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues?sort=updated_desc&state=opened&label_name[]=still+relevant?&
* [x] Documentation tickets: https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues?sort=updated_desc&state=opened&label_name[]=Documentation
* [ ] Deployment tickets: https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues?sort=updated_desc&state=opened&label_name[]=Deployment


### Review Project Phases and Milestones

* TUF Implementation Scope Overview: https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/10
* Release milestones are here now: https://gitlab.com/rugged/rugged/-/milestones?sort=due_date_desc&state=all
    * Remove from da/tuf? see: https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/milestones?search_title=Release&state=all&sort=due_date_desc
        * Fine to remove and keep only in rugged/rugged
    * Also, they're out-of-date
* Phase timelines are out-of-date: https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/milestones?search_title=Phase&state=all&sort=due_date_desc
    * We will update these to our newly adjusted timeline, per discussions in prior sprints. 


### Sustainability of Rugged in the long-term


