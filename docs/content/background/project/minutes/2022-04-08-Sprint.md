---
title: Sprint Review & Planning - 2022-04-08
menutitle: 2022-04-08 Sprint Review
hidden: true
date: 2022-04-08

---

Project docs: https://drupal-infrastructure.gitlab.io/package-signing/tuf
Etherpad: https://pad.riseup.net/p/da-tuf-sprint-keep

## Check-in (5m)

(we don't usually take notes for this section)

## Admin (5m)

A - Attendance

* Invitees: Tim, Christopher
* Attendees: Tim, Christopher
* Regrets: None

D - Duration (55m)

M - Minutes: https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-04-01-sprint/

I - Information
* Neil has been working on updating the Packaging Pipeline to prepare a 'slot' for TUF to plug into (moving composer processing, DCI triggering) 

N - Next Meeting (Weekly Fridays @ 3:30PM EST)
* 2022-04-14 @3:30PM EST (Thursday, due to Good Friday holiday)

## Backlog

TODO:
* [x] (Christopher) Implement gitlab link shortcode.
* [x] (Christopher) Want to add test to add targets, publish, then add again and publish - (cache checking)
* [ ] (Christopher) Add a ticket for publishing the verification keys to the package repo.  <-- May not be needed
* [ ] (Christopher) Add a ticket for: Document known distinctions between packaging pipeline and rugged responsibilities.
* [ ] (Christopher) Review `RuggedRepository()` @TODOs (some not needed for our usecase)
    * {{< gitlab 35 "Key rotation" >}}
    * {{< gitlab 97 "Sig threshold config per key" >}}
    * {{< gitlab 98 "Expiration config per role" >}}
    * {{< gitlab 99 "Hashed bins" >}}- likely want for go-live
    * {{< gitlab 100 "Consistent snapshots" >}} - likely not needed, needs client option
    * {{< gitlab 36 "Delegated targets" >}} - not on critical path
* [ ] (Christopher) Begin thinking about transferring project to official 'Rugged' repo and publishing to pip
    * Need to consider where to put things that are D.O specific - like containers, or other config.

## Agenda

### Sprint Review (15m)

* What have we accomplished?
    * Completed {{< gitlab 91 "Isolate keys used in workers from each other" >}}
        * Sync'd all the places we need to track this (`.gitlab-ci.yml`, `docker-compose.*.yml`, `rugged.worker` Ansible role.
    * Completed {{< gitlab 96 "Implement linting" >}}
    * Completed {{< gitlab 86 "Move to modern API" >}}
        * Re-wrote almost everything, but it's so much better now.
        * This was a heavy lift.
    * Coordinated some with w/ @tedbow and @phenaproxima.

* What have we learned?
    * Tests proved invaluable in major refactor.

* Are we ready for Beta?
    * Yes! (Pending a few small remaining tasks, and coordination with team)

**PROPOSAL**: Accept the tickets completed as documented above as completion for Sprint #13 - **CONSENTED**


### Sprint Retrospective (10m)

* What did we do particularly well?
    * Implementing our own repository class greatly simplified everything.
    * Now that @tedbow and @phenaproxima are actively working on `php-tuf` and the Composer plugin, risks of incompatibility, etc. are greatly reduced.

* What could we have done better?
    * Possibly over-specifying output/logging in tests
    * Still haven't caught up on docs.

* Known risks or items to watch?
    * php-tuf client complies with an older verion of Python-TUF. Need to test further and coordinate w/ @tedbow and @phenaproxima.
    * Maybe, but probably not: an additional test, mounting dirs as read-only and confirming access (or add a new container without r/w access) 


### Sprint Planning (10m)

* What do we intend to accomplish?
    * Complete {{< gitlab 95 "Extend test coverage" >}} (@WIP tests)
    * Catch up on docs.
    * Begin regrouping with key stakeholders for beta (drumm, tedbow, phenaproxima)

* What do we need help with?
    * Re-planning next steps, release cycles, etc.
    * Triage and backlog grooming with Tim

* Road towards production
    * Per last convo with drumm - once we have finalized beta, schedule pairing to figure where to deploy these things.
        * I *think* we're ready to proceed with this
    * @tedbow planning autoupdates BOF. Ideally we'll have a beta TUF service in place.

**PROPOSAL**: Proceed with above high level project plan changes as well as scope for Sprint 14 **CONSENTED**


## Next meeting
* Apr 14, 2021
    * N.B. [Beta release](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/milestones/6#tab-issues) 
        * New beta target date is likely to be: Apr 08 (kinda)
        * Should groom backlog for nice-to-haves and obsolete tickets (on Tues)
