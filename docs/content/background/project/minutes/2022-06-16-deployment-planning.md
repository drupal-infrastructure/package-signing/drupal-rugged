---
title: Rugged TUF Deployment Planning - 2022-06-16
menutitle: 2022-06-16 Deployment Planning
hidden: true
date: 2022-06-16

---

Project docs: https://drupal-infrastructure.gitlab.io/package-signing/tuf
Sprint review and planning pad: https://pad.riseup.net/p/da-tuf-sprint-keep
Project review pad: https://pad.riseup.net/p/da-tuf-project-review
Deployment planning pad: https://pad.riseup.net/p/da-tuf-deployment

## Check-in (5m)

(we don't usually take notes for this section)

## Admin (5m)

A - Attendance

* Invitees: Christopher, Neil
* Attendees: Christopher, Neil
* Regrets: None

D - Duration (55m)

M - Minutes: https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-05-24-deployment-planning/

I - Information

N - Next Meeting
* 2022-06-21 @4:00PM EST

## Backlog

## Agenda

### Coordinate re. deployment

* [ ] Document hosting requirements - so we can communicate with Narayan/Tag1
    * Generate list of questions for Narayan

Previous discussion:
- https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-05-24-deployment-planning/


performance testing/benchmarking:
* lots of small files
* several large files
* lots of large files

* use webform as example of a big package


DRAFT tickets for setting up various environments:

* [#105: Requirements for ADMIN environment](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/105)
* [#104: Requirements for PACKAGING PIPELINE environment](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/104)
* [#103: Requirements for WORKER environments](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/103)
