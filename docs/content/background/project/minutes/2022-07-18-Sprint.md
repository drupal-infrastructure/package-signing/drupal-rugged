---
title: Sprint Review & Planning - 2022-07-18
menutitle: 2022-07-18 Sprint Review
hidden: true
date: 2022-07-18

---

Project docs: https://drupal-infrastructure.gitlab.io/package-signing/tuf
Sprint review and planning pad: https://pad.riseup.net/p/da-tuf-sprint-keep
Project review pad: https://pad.riseup.net/p/da-tuf-project-review
Deployment planning pad: https://pad.riseup.net/p/da-tuf-deployment

## Check-in (5m)

(we don't usually take notes for this section)

## Admin (5m)

A - Attendance

* Invitees: Tim, Christopher, Neil
* Attendees: Tim, Christopher,  Neil
* Regrets: None

D - Duration (55m)

M - Minutes: https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-06-20-sprint (awaiting packaging retry)

I - Information
* Still coming back from vacation and getting reintegrated
* AutoUpdates could go in as early as 10.0 - would need to have infra stood up by Aug 12 - for Sep 12 beta window. 
        * 10.1 would be exactly six months more

N - Next Meeting (Weekly Mondays @ 4:00PM EST)
* 2022-07-25 @4:00PM EST

## Backlog

* [x] Reconsider if Rugged is ready to circulate with the TUF community
    * Tim mentioned Rugged in a OpenSSF, then Trishank mentioned it in the #TUF Slack channel.
* [x] (Christopher) publish https://pad.riseup.net/p/da-tuf-deployment

## Agenda

### Sprint Review (15m)

* What have we accomplished?
    * @nnewton has done a review of the container architecture (see sprint planning below for 2 requests) and started to look into building a helm chart for the whole thing.
        * See [Note on #103](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/103#note_1029839302)
    * Requirements defined in a few issues, to make the infra stand-up more grokable 
        * [#103 - Requirements for Worker envs](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/103)
        * [#104 - Requirements for Packaging Pipeline](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/104)
        * [#105 - Requirements for Admin envs](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/105)
            * Might have additional requirements depending on where it's running (local env or other) - could do equivalent as a composer template project to deploy into a local ddev
            * Is local key rotation a requirement?
            * Do we want HSMs? If so - only the Root key
    * Made some progress on (#113 - versioning for root metadata)[https://gitlab.com/rugged/rugged/-/issues/113]

* What have we learned?
    * The requirements issues were a good summary of what is needed to get the conatiners up and running in a prod like environment (@nnewton was struggling to figure out where to dive in from pure docs before we wrote the issues)
    * One option to move forward with key rotation - maybe ssh into root worker env with strict access controls. Could keep root threshold at 1, each new key can sign the others. (to do: validate with @dts) 
            * As long as we can raise the threshold in future if we need to, we can start with a 1 threshold. This is possible, but more engineering might be required to change midstream - the repo supports it, but the multiple key signing the new key feature isn't yet built because it's not yet required. 

**PROPOSAL**: Accept the tickets completed as documented above as completion for this sprint - **CONSENTED**


### Sprint Retrospective (10m)

* What did we do particularly well?
    * Rejuvinating during Vacation!
    * Identifying specific needs/next-steps/blockers for infra team (103-105) 

* What could we have done better?
    * @drumm and @hestenet could have spent more time with @nnewton, but given how busy the last few weeks were, I think we got what we needed to keep moving. 

* Known risks or items to watch?
    * php-tuf client complies with an older verion of Python-TUF. Need to test further and coordinate w/ @tedbow and @phenaproxima. (relates to #114 - consistent snapshots)
        * Started to file tickets against php-tuf library for any potential incompatibilities - highlighted a fix in #115 as well

### Sprint Planning (10m)

* What do we intend to accomplish?
    * Request for support with getting the container permissions right as Narayan builds out the helm chart: 
        * [#103 - Requirements for Worker envs](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/103)
        * [#105 - Requirements for Admin envs](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/issues/105)
            * Architectural decision: Assume it's running as part of the k8s container cloud for now. For pre-release purposes this will let us accomplish the rest of our testing with AutoUpdates team - at this point won't need high root key security.
            * So this is the same as the spec for the root container, except that it doesn't need to be running the root worker. Instead it needs to be able to run `rugged` CLI.
                * Final decision on having it in k8s vs. local ddev vs something else will be based on security posture. TO DO: Discuss with @dts and any other relevant others.
                * Maybe we should look at integrating with some kind of key management service (AWS thing rather than NFS mounts?)
                    * To accommodate this, we may want to update the rotate-keys command to allow external generation of the new key, rather than self-generation
    * Complete [#108: Ensure that roles don't have stale keys](https://gitlab.com/rugged/rugged/-/issues/108), which implies:
        * [#113: Enable file versioning for root metadata](https://gitlab.com/rugged/rugged/-/issues/113)

* What do we intend to accomplish next?
    * [#111: Ensure that Rugged config can be injected via ENV vars](https://gitlab.com/rugged/rugged/-/issues/111)
    * [#112: Document refresh cronjob](https://gitlab.com/rugged/rugged/-/issues/112)
    * [#114: Revert to upstream Composer plugin](https://gitlab.com/rugged/rugged/-/issues/114)
        * [#115: Add hash of `snapshot.json` in timestamp metadata](https://gitlab.com/rugged/rugged/-/issues/115)
    * [#106: OSX support](https://gitlab.com/rugged/rugged/-/issues/106)
    * (as time permits) Additional support for phenaproxima as needed:
        * Christopher to try to reproduce error on his own OSX laptop.

* What do we need help with?
    * Fleshing out the intro and usage docs.
    * Sustainable funding for Rugged
        * Finding things organizations know they should fund: security audit, supply chain sescurity
        * Feature bounty 
        * Co-marketing 

Target: Try to have beta infra running by Aug 12th.

**PROPOSAL**: Proceed with scope for the upcoming sprint **CONSENTED**


