---
title: Rugged TUF Deployment Planning - 2022-05-24
menutitle: 2022-05-24 Deployment Planning
hidden: true
date: 2022-05-24

---

Project docs: https://drupal-infrastructure.gitlab.io/package-signing/tuf
Sprint review and planning pad: https://pad.riseup.net/p/da-tuf-sprint-keep
Project review pad: https://pad.riseup.net/p/da-tuf-project-review
Deployment planning pad: https://pad.riseup.net/p/da-tuf-deployment

## Check-in (5m)

(we don't usually take notes for this section)

## Admin (5m)

A - Attendance

* Invitees: Christopher, Neil
* Attendees: Christopher, Neil
* Regrets: None

D - Duration (55m)

M - Minutes: https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-05-18-deployment-planning/

I - Information

N - Next Meeting
* 2022-05-24 @4:00PM EST

## Backlog

## Agenda

### Coordinate re. deployment

* [ ] Document hosting requirements - so we can communicate with Narayan/Tag1
    * Generate list of questions for Narayan

Notes:
* https://gitlab.com/rugged/rugged/-/blob/main/.gitlab-ci.yml we run docker in docker, has an example of containers & mounts enumerated
* Test suite puts it all in one place, production will be more sectioned-off & limited access to each piece
* root-worker is more of an admin container, which might even be run locally or somewhere on a VPN that can be accessed for maintenance (locally so root key doesn't go over the internet)
* Some mounts will be :ro, read-only in the future
* Packer (image building)
    * https://gitlab.com/rugged/rugged/-/tree/main/build/packer/scripts bootstraps ansible
    * https://gitlab.com/rugged/rugged/-/tree/main/build/packer/docker container build definitions
* Ansible (config management)
    * https://gitlab.com/rugged/rugged/-/tree/main/build/ansible
    * https://gitlab.com/rugged/rugged/-/tree/main/build/ansible/roles/rugged.workers setting up supervisord

* Main repo builds containers
* Separate -infra repo with Association-specific things, terraform, etc
* Secrets management, such as RabbitMQ/celery credentials, currently is baked into images, probably should set the real credentials when the containter is started
* Not currently in env variables, stored in yaml files. It is potentially possible to pick up environment variables, will need research. (Could have supervisord use the environment varible directly, too.)
* Get the ro/rw access to (snapshot/target/timestamp pulic/private keys), (incoming targets to be signed, resulting TUF repository) mounts, correct access for each containter type. Needs to be externally accessible for rotations, admin container runs locally and updates them.
* How to host RabbitMQ - own container / AWS-managed service? Locally-run admin container should have access as well.
* Admin container
* Runs locally for rotating keys
* Rugged running in CLI mode
* Mount shared drives, ansible sets this up currently
* Might be custom-built container for this part
* Get (modify?) shared config
* Need to decide on security stance
* HSM? Is that overkill
* Rotation schedule
