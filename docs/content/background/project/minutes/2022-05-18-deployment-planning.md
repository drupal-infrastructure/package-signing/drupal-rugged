---
title: Rugged TUF Deployment Planning - 2022-05-18
menutitle: 2022-05-18 Deployment Planning
hidden: true
date: 2022-05-18

---

Project docs: https://drupal-infrastructure.gitlab.io/package-signing/tuf
Sprint review and planning pad: https://pad.riseup.net/p/da-tuf-sprint-keep
Project review pad: https://pad.riseup.net/p/da-tuf-project-review
Deployment planning pad: https://pad.riseup.net/p/da-tuf-deployment

## Check-in (5m)

(we don't usually take notes for this section)

## Admin (5m)

A - Attendance

* Invitees: Christopher, Neil
* Attendees: Christopher, Neil
* Regrets: None

D - Duration (55m)

M - Minutes: https://drupal-infrastructure.gitlab.io/package-signing/tuf/background/project/minutes/2022-05-06-sprint

I - Information

N - Next Meeting
* 2022-05-24 @4:00PM EST

## Backlog

## Agenda

### Coordinate re. deployment

* [x] Start with - deploy a local dev env w/ Docker on Drumm's local as a walkthrough
* [ ] Document hosting requirements - so we can communicate with Narayan/Tag1
    * Generate list of questions for Narayan
* [x] Review configuration management - Ansible (for user accounts and similar)
* [x] Review shared filesystem structure

Notes:
* https://gitlab.com/rugged/rugged/-/blob/main/.gitlab-ci.yml we run docker in docker, has an example of containers & mounts enumerated
* Test suite puts it all in one place, production will be more sectioned-off & limited access to each piece
* root-worker is more of an admin container, which might even be run locally or somewhere on a VPN that can be accessed for maintenance (locally so root key doesn't go over the internet)
* Some mounts will be :ro, read-only in the future
* Packer (image building)
    * https://gitlab.com/rugged/rugged/-/tree/main/build/packer/scripts bootstraps ansible
    * https://gitlab.com/rugged/rugged/-/tree/main/build/packer/docker container build definitions
* Ansible (config management)
    * https://gitlab.com/rugged/rugged/-/tree/main/build/ansible
    * https://gitlab.com/rugged/rugged/-/tree/main/build/ansible/roles/rugged.workers setting up supervisord
