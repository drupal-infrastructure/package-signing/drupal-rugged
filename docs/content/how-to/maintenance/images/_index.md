---
title: Building Docker images with Packer
menutitle: Build container images
weight: 10

---

### How to (re)build container images

This project uses Docker container images for modeling a networked architecture
akin to the production environment.

First, you may need to install some depencencies:

```
brew install nss      # for mkcert
brew install python
# (add to PATH, alias python to python3)
pip3 install jinja2
brew install ansible@2.9
```

To rebuild the container images and push them up to the container registry,
run:

```
source d # Bootstrap Drumkit
make ci-images
```

This will trigger [Packer](https://packer.io) to build each of the project's
container images, running the relevant provisioning scripts, then tagging the
resulting images and finally pushing them up to the project container registry.


### Individual images

If you want to simply build a specific subset of the container images, you can
do so by targeting them individually:

* `make docker/rabbitmq` - build and push the `rabbitmq` image
* `make docker/packaging-pipeline` - build and push the `packaging-pipeline` image
* `make docker/test-worker` - build and push the `test-worker` image

### More info

For more information, see the [background information on Docker images](../../background/images) and [reference section on Docker container images](../../reference/images).
