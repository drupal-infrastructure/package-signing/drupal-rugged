---
title: "rugged generate-keys"
hidden: True
---

#### Command help text
```terminal
$ rugged generate-keys --help

Usage: rugged generate-keys [OPTIONS]

  Generate a keypair for verification and signing of TUF metadata.

Options:
  --role TEXT  The specific role for which to generate a keypair. Can be
               passed multiple times.
  --local      Generate keys locally, rather than delegating to the root
               worker.
  --help       Show this message and exit.
```

#### Command example output
```terminal
$ rugged generate-keys 

Generating 'root' keypair for 'root' role.
Generating 'root1' keypair for 'root' role.
Generating 'snapshot' keypair for 'snapshot' role.
Generating 'targets' keypair for 'targets' role.
Generating 'timestamp' keypair for 'timestamp' role.
Signing key:       /var/rugged/signing_keys/root/root
Verification key:  /var/rugged/verification_keys/root.pub

Signing key:       /var/rugged/signing_keys/root/root1
Verification key:  /var/rugged/verification_keys/root1.pub

Signing key:       /var/rugged/signing_keys/snapshot/snapshot
Verification key:  /var/rugged/verification_keys/snapshot.pub

Signing key:       /var/rugged/signing_keys/targets/targets
Verification key:  /var/rugged/verification_keys/targets.pub

Signing key:       /var/rugged/signing_keys/timestamp/timestamp
Verification key:  /var/rugged/verification_keys/timestamp.pub

```
