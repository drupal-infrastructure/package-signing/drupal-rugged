---
title: "rugged remove-targets"
hidden: True
---

#### Command help text
```terminal
$ rugged remove-targets --help

Usage: rugged remove-targets [OPTIONS] [TARGETS]...

  Remove targets from a TUF repository.

Options:
  --help  Show this message and exit.
```

#### Command example output
```terminal
$ rugged remove-targets example.txt

Removed the following targets from the repository:
example.txtUpdated targets metadata.
Updated snapshot metadata.
Updated timestamp metadata.
```
