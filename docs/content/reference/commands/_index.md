---
title: Rugged commands
menuTitle: Commands
weight: 40
---

Rugged CLI commands may be triggered by TUF repository administrators directly, or via the packaging pipeline. These will include:

{{< children depth=3 showhidden=1 >}}

#### Base command help text

```terminal
$ rugged --help

Usage: rugged [OPTIONS] COMMAND [ARGS]...

  A CLI tool for the Rugged TUF Server.

Options:
  --version            Show the version and exit.
  -v, --verbosity LVL  Either CRITICAL, ERROR, WARNING, INFO or DEBUG
  --debug              Output and log debug messages.
  --help               Show this message and exit.

Commands:
  add-targets     Add targets to a TUF repository.
  config          Print the config for a TUF repository.
  echo            Ping a worker, by sending an echo task on the queue.
  generate-keys   Generate a keypair for verification and signing of TUF...
  initialize      Initialize a new TUF repository.
  logs            Print the logs for a TUF repository.
  remove-targets  Remove targets from a TUF repository.
  status          Print a status message for a TUF repository.
```
