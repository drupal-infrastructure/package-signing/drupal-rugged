---
title: "Feature: Command to print repo status."
hidden: True
---

#### Test results for [`features/commands/status.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/commands/status.feature)
Running `behat features/commands/status.feature` results in:

```gherkin
@rugged @commands @status @test-worker
Feature: Command to print repo status.
  In order to ensure that the repo is working properly
  As an administrator
  I need to read the status of the repo.

  Background:
    Given I rebuild fixtures

  Scenario: Print status of a non-functional repo.
    Given I reset Rugged
    When I fail to run "sudo -u rugged rugged status"
    Then I should get:
      """
      error: TypeError thrown in _get_repo_keys_for_root_role: 'bool' object is not subscriptable
      error: RuggedMetadataError thrown in __init__: Failed to generate key metadata during TUF repository initialization.
      error: Failed to instantiate repository.
      error: RuggedRepositoryError thrown in status_cmd: The repository could not load a repository at the given path.
      error: Failed to instantiate repository.
      Check the logs for more detailed error reporting.
      """

  Scenario: Print local status.
    When I run "sudo -u rugged rugged status --local"
    Then I should get:
      """
      === Repository status for local operations ===
        Targets  Total Size
      ---------  ------------
              3  228.3 kB
      
      Role       Capability    Signatures      Version  TUF Spec    Expires
      ---------  ------------  ------------  ---------  ----------  -------------------
      targets    Signing       1 / 1                 2  1.0.28
      snapshot   Signing       1 / 1                 2  1.0.28
      timestamp  Signing       1 / 1                 2  1.0.28
      root       Signing       2 / 1                 1  1.0.28
      
      Key name    Role       Key type(s)      Scheme    Path
      ----------  ---------  ---------------  --------  --------------------------------------------
      targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
      snapshot    snapshot   public, private  ed25519   /var/rugged/signing_keys/snapshot/snapshot
      timestamp   timestamp  public, private  ed25519   /var/rugged/signing_keys/timestamp/timestamp
      root        root       public, private  ed25519   /var/rugged/signing_keys/root/root
      root1       root       public, private  ed25519   /var/rugged/signing_keys/root/root1
      """
    Then I should not get:
      """
      === Repository status for root-worker ===
      === Repository status for targets-worker ===
      === Repository status for snapshot-worker ===
      === Repository status for test-worker ===
      === Repository status for timestamp-worker ===
      """

  Scenario: Print a worker's status.
    When I run "sudo -u rugged rugged status --worker=test-worker"
    Then I should get:
      """
      === Repository status for test-worker ===
        Targets  Total Size
      ---------  ------------
              3  228.3 kB
      
      Role       Capability    Signatures      Version  TUF Spec    Expires
      ---------  ------------  ------------  ---------  ----------  -------------------
      targets    Signing       1 / 1                 2  1.0.28
      snapshot   Verification  1 / 1                 2  1.0.28
      timestamp  Verification  1 / 1                 2  1.0.28
      root       Verification  2 / 1                 1  1.0.28
      
      Key name    Role       Key type(s)      Scheme    Path
      ----------  ---------  ---------------  --------  -------------------------------------------
      targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
      snapshot    snapshot   public           ed25519   /var/rugged/verification_keys/snapshot.pub
      timestamp   timestamp  public           ed25519   /var/rugged/verification_keys/timestamp.pub
      root        root       public           ed25519   /var/rugged/verification_keys/root.pub
      root1       root       public           ed25519   /var/rugged/verification_keys/root1.pub
      """
    Then I should not get:
      """
      === Repository status for local operations ===
      === Repository status for root-worker ===
      === Repository status for snapshot-worker ===
      === Repository status for targets-worker ===
      === Repository status for timestamp-worker ===
      """

  Scenario: Print multiple workers' statuses.
    When I run "sudo -u rugged rugged status --worker=test-worker --worker=targets-worker"
    Then I should get:
      """
      === Repository status for test-worker ===
      === Repository status for targets-worker ===
      """
    Then I should not get:
      """
      === Repository status for local operations ===
      === Repository status for root-worker ===
      === Repository status for snapshot-worker ===
      === Repository status for timestamp-worker ===
      """

  Scenario: Print overall status.
    When I run "sudo -u rugged rugged status"
    Then I should get:
      """
      === Repository status for local operations ===
      === Repository status for root-worker ===
      === Repository status for snapshot-worker ===
      === Repository status for targets-worker ===
      === Repository status for test-worker ===
      === Repository status for timestamp-worker ===
      """

5 scenarios (5 passed)
19 steps (19 passed)
```
