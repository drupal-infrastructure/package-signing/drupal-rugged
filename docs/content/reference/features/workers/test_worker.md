---
title: "Feature: A test worker to validate queue/RPC architecture."
hidden: True
---

#### Test results for [`features/workers/test_worker.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/workers/test_worker.feature)
Running `behat features/workers/test_worker.feature` results in:

```gherkin
@rugged @workers @test-worker
Feature: A test worker to validate queue/RPC architecture.
  In order to ensure that the queue system works
  As a developer
  I need to ensure that a test worker can run tasks.

  Background:
    Given I reset Rugged

  Scenario: Send a basic ping/echo message.
    When I run "sudo -u rugged rugged echo --worker=test-worker --timeout=1"
    Then I should get:
      """
      Sending test-worker Ping!...
      Done. Response was: test-worker PONG: Ping!
      """

  Scenario: Retrieve worker logs.
    Given I run "sudo -u rugged rugged echo --worker=test-worker --timeout=1"
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

2 scenarios (2 passed)
7 steps (7 passed)
```
