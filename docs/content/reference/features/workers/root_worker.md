---
title: "Feature: Root worker to run privileged operations."
hidden: True
---

#### Test results for [`features/workers/root_worker.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/workers/root_worker.feature)
Running `behat features/workers/root_worker.feature` results in:

```gherkin
@rugged @workers @root-worker
Feature: Root worker to run privileged operations.
  In order to securely run privileged operations on a TUF repo
  As an administrator
  I need to dispatch tasks to a 'root' worker.

  Background:
    Given I reset Rugged

  Scenario: Send a basic ping/echo message.
    When I run "sudo -u rugged rugged echo --worker=root-worker --timeout=1"
    Then I should get:
      """
      Sending root-worker Ping!...
      Done. Response was: root-worker PONG: Ping!
      """

  Scenario: Retrieve worker logs.
    Given I run "sudo -u rugged rugged echo --worker=root-worker --timeout=1"
    When I run "sudo -u rugged rugged logs --worker=root-worker"
    Then I should get:
      """
      INFO (base_worker.echo): root-worker received echo task: Ping!
      """

2 scenarios (2 passed)
7 steps (7 passed)
```
