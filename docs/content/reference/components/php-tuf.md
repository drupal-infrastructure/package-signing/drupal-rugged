---
title: PHP-TUF Library
weight: 20

---

[PHP-TUF](https://github.com/php-tuf/php-tuf) is a PHP implementation of [The Update Framework (TUF)](/package-signing/tuf/reference/tuf/) to provide signing and verification for secure PHP application updates.

**N.B.** [`fixtures/builder.py`](https://github.com/php-tuf/php-tuf/blob/main/fixtures/builder.py) provides a high-level interface to many TUF operations.

## Documentation

- [Root `README`](https://github.com/php-tuf/php-tuf/blob/main/README.md)
- [Project wiki](https://github.com/php-tuf/php-tuf/wiki)
- [Dependencies](https://github.com/php-tuf/php-tuf/blob/main/DEPENDENCIES.md)
- [Initial proposal](../PHP-TUF.pdf)
- [Automated tests](https://github.com/php-tuf/php-tuf/actions) and related [config](https://github.com/php-tuf/php-tuf/blob/main/.github/workflows/php.yml).
