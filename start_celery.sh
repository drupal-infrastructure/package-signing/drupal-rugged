#!/bin/bash
set -x
set -o errexit nounset pipefail

cd /usr/local/bin

# Start the monitor worker with a scheduler (Celery Beat).
celery --app="$RUGGED_WORKER" worker --concurrency=1 --loglevel=INFO --queues="$RUGGED_WORKER" --task-events --beat --schedule=/tmp/monitor_schedule
