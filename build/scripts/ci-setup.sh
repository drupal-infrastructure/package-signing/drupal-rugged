#! /bin/sh

# Override the deployed 'Rugged' package with a virtual environment.
sudo sudo -u rugged pip install -e .
sudo sudo pipenv install --dev

# Create a non-root user, since sudo'ing to `rugged` is part of our tests.
useradd non-root-test-user --create-home --groups rugged
sudo chown -R non-root-test-user:non-root-test-user d9-site/ satis/

# We need a more recent version of Composer than ships by default, in order to use the latest Satis.
composer self-update

# Ownership of `/opt/rugged` seems to be strange in CI.
git config --global --add safe.directory /opt/rugged
