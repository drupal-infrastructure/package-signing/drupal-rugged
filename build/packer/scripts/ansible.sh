#!/bin/sh -eux

# Install Ansible and related utilities.

export DEBIAN_FRONTEND=noninteractive

python3 -m pip install jinja2-cli matrix-client ansible
