COMPOSER_TUF_DIR      ?= d9-site/tuf
RUGGED_HOME           ?= /var/rugged
RUGGED_CONFIG_DIR     ?= $(RUGGED_HOME)/.config
TUF_REPO_DIR          ?= $(RUGGED_HOME)/tuf_repo
TUF_REPO_METADATA_DIR ?= $(TUF_REPO_DIR)/metadata
TUF_REPO_TARGETS_DIR  ?= $(TUF_REPO_DIR)/targets
PKG_REPO_DIR          ?= $(RUGGED_HOME)/pkg_repo
PKG_REPO_METADATA_DIR ?= $(PKG_REPO_DIR)/metadata
PKG_REPO_TARGETS_DIR  ?= $(PKG_REPO_DIR)/targets
INBOUND_TARGETS_DIR   ?= $(RUGGED_HOME)/incoming_targets
POST_TO_TUF_DIR       ?= /opt/post_to_tuf

VERIFICATION_KEY_DIR           ?= $(RUGGED_HOME)/verification_keys
ROOT_VERIFICATION_KEY_DIR      ?= $(VERIFICATION_KEY_DIR)/root
TIMESTAMP_VERIFICATION_KEY_DIR ?= $(VERIFICATION_KEY_DIR)/timestamp
SNAPSHOT_VERIFICATION_KEY_DIR  ?= $(VERIFICATION_KEY_DIR)/snapshot
TARGETS_VERIFICATION_KEY_DIR   ?= $(VERIFICATION_KEY_DIR)/targets
VERIFICATION_KEY_DIRS          ?= $(ROOT_VERIFICATION_KEY_DIR) $(TIMESTAMP_VERIFICATION_KEY_DIR) $(SNAPSHOT_VERIFICATION_KEY_DIR) $(TARGETS_VERIFICATION_KEY_DIR)

SIGNING_KEY_DIR           ?= $(RUGGED_HOME)/signing_keys
ROOT_SIGNING_KEY_DIR      ?= $(SIGNING_KEY_DIR)/root
TIMESTAMP_SIGNING_KEY_DIR ?= $(SIGNING_KEY_DIR)/timestamp
SNAPSHOT_SIGNING_KEY_DIR  ?= $(SIGNING_KEY_DIR)/snapshot
TARGETS_SIGNING_KEY_DIR   ?= $(SIGNING_KEY_DIR)/targets
SIGNING_KEY_DIRS          ?= $(ROOT_SIGNING_KEY_DIR) $(TIMESTAMP_SIGNING_KEY_DIR) $(SNAPSHOT_SIGNING_KEY_DIR) $(TARGETS_SIGNING_KEY_DIR)

RUGGED_KEY_DIRS = $(VERIFICATION_KEY_DIR) $(VERIFICATION_KEY_DIRS) $(SIGNING_KEY_DIR) $(SIGNING_KEY_DIRS)
RUGGED_DIRS     = $(TUF_REPO_DIR) $(RUGGED_CONFIG_DIR) $(INBOUND_TARGETS_DIR) $(POST_TO_TUF_DIR) $(RUGGED_KEY_DIRS)

.PHONY: fixtures register-tuf-repo publish-tuf-metadata init-rugged-repo clean-rugged reset-rugged clean-rugged-artifacts sign-package-repo-files

fixtures: init-rugged-repo publish-tuf-metadata register-tuf-repo  ##@rugged Create test/dev fixtures by re-building Satis and Rugged repo from scratch.
clean-fixtures: clean-rugged-artifacts clean-satis-artifacts

register-tuf-repo: $(COMPOSER_TUF_DIR)/packages.ddev.site.json
$(COMPOSER_TUF_DIR):
	$(ddev_exec) mkdir -p $@
$(COMPOSER_TUF_DIR)/packages.ddev.site.json: $(COMPOSER_TUF_DIR)
	$(ddev_exec) cp $(PKG_REPO_METADATA_DIR)/1.root.json $@

publish-tuf-metadata: sign-package-repo-files
	$(ddev_exec) cp -r $(TUF_REPO_METADATA_DIR) $(PKG_REPO_METADATA_DIR)
	$(ddev_exec) cp -r $(TUF_REPO_TARGETS_DIR) $(PKG_REPO_TARGETS_DIR)

init-rugged-repo: reset-rugged
	$(rugged) generate-keys --local
	$(rugged) initialize --local

clean-rugged:
	$(ddev_sudo) rm -rf rugged.egg-info/

reset-rugged: clean-rugged-artifacts
	$(ddev_sudo) mkdir -p $(RUGGED_CONFIG_DIR)/rugged  # Ensure there is a config directory.
	$(rugged) logs --truncate                          # Truncate all the logs.
	for RUGGED_DIR in $(TUF_REPO_DIR) $(INBOUND_TARGETS_DIR) $(POST_TO_TUF_DIR); do \
            $(ddev_sudo) chmod -R a=r,ug+w,a+X $$RUGGED_DIR ; \
        done
	for RUGGED_DIR in $(RUGGED_KEY_DIRS) ; do \
            $(ddev_sudo) chmod -R a-rwx,ug+rw,a+X $$RUGGED_DIR ; \
        done
	for RUGGED_DIR in $(RUGGED_DIRS) ; do \
            $(ddev_sudo) chown -R rugged:`whoami` $$RUGGED_DIR ; \
        done
	$(ddev_sudo) chown -R rugged:`whoami` $(INBOUND_TARGETS_DIR)

clean-rugged-artifacts:
	# @TODO: Figure out how to better handle mis-alignment b/w the local
	# host user and the one in the DDEV container.
	for RUGGED_DIR in $(RUGGED_DIRS) ; do \
            $(ddev_sudo) chmod -f --silent -R a=r,ug+w,a+X $$RUGGED_DIR ; \
            $(ddev_sudo) chown -f --silent -R rugged:`whoami` $$RUGGED_DIR ; \
        done
	# Tell Git that the ownership of /var/www/html in DDEV isn't relevant.
	# See: https://weblog.west-wind.com/posts/2023/Jan/05/Fix-that-damn-Git-Unsafe-Repository
	$(ddev_exec) git config --global --add safe.directory /var/www/html
	$(ddev_exec) git checkout fixtures
	$(ddev_exec) git clean -fxd fixtures
	$(ddev_sudo) rm -rf $(RUGGED_HOME)/.config/rugged/*

sign-package-repo-files: build-satis-repo $(INBOUND_TARGETS_DIR)/drupal/token/1.9.0.0 $(INBOUND_TARGETS_DIR)/drupal/token/1.8.0.0 $(INBOUND_TARGETS_DIR)/packages.json
	$(rugged) --debug add-targets
$(INBOUND_TARGETS_DIR)/drupal/token/1.9.0.0:
	$(ddev_sudo_rugged) mkdir -p $(@D)
	$(ddev_sudo_rugged) curl -o $@ https://ftp.drupal.org/files/projects/token-8.x-1.9.zip
$(INBOUND_TARGETS_DIR)/drupal/token/1.8.0.0:
	$(ddev_sudo_rugged) mkdir -p $(@D)
	$(ddev_sudo_rugged) curl -o $@ https://ftp.drupal.org/files/projects/token-8.x-1.8.zip
$(INBOUND_TARGETS_DIR)/packages.json:
	$(ddev_sudo_rugged) cp $(PKG_REPO_DIR)/packages.json $@
	$(ddev_sudo_rugged) cp $(PKG_REPO_DIR)/include $(@D) -r

build-satis-repo: satis reset-satis
	$(ddev_exec) satis/bin/satis build satis/satis.json

reset-satis: clean-satis-artifacts clean-composer-artifacts satis-composer-file

clean-satis-artifacts:
	$(ddev_exec) rm -rf $(PKG_REPO_DIR)/* $(PKG_REPO_DIR)/.[!.][!gitkeep]*

clean-composer-artifacts:
	rm -f d9-site/composer.*
	rm -rf d9-site/vendor
	rm -rf d9-site/web
	rm -rf d9-site/tuf

satis-composer-file:
	cd d9-site; ln -fs satis-composer.json composer.json
