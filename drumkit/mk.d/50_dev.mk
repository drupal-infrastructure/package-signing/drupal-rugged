.PHONY: dev packaging-pipeline $(RUGGED_WORKERS) redeploy clean-upstream fix-upstream

RUGGED_WORKERS = monitor-worker root-worker snapshot-worker targets-worker test-worker timestamp-worker

# Update dependencies installed system-wide, since that's what'll be used for tests, including dev dependencies.
RUGGED_SYNC_DEPENDENCIES_CMD = cd /opt/rugged; sudo pipenv sync --dev --system
RUGGED_DEPLOY_PACKAGE_CMD = sudo pip3 install /opt/rugged -qq
RUGGED_UPGRADE_PIP_CMD = sudo pip3 install --upgrade pip

dev: clean-fixtures
dev: packaging-pipeline
dev: $(RUGGED_WORKERS)
dev: redeploy
dev: ##@rugged Override the deployed 'Rugged' package with the latest code.

benchmarks:
	@$(behat) --stop-on-failure --profile=benchmarks

update-dependencies:
	@$(ddev) exec "cd /opt/rugged; sudo pipenv update"

packaging-pipeline:
	@$(ECHO) "$(YELLOW)Updating Rugged's dependencies on $@.$(RESET)"
	@$(ddev) exec "$(RUGGED_UPGRADE_PIP_CMD)"
	@$(ddev) exec "$(RUGGED_SYNC_DEPENDENCIES_CMD)"

$(RUGGED_WORKERS):
	@$(ECHO) "$(YELLOW)Updating $@.$(RESET)"
	@$(ddev) -s $@ exec "$(RUGGED_UPGRADE_PIP_CMD)"
	@$(ddev) -s $@ exec "$(RUGGED_SYNC_DEPENDENCIES_CMD)"

redeploy-rugged-cli:
	@$(ECHO) -e "$(YELLOW)Redeploying code to packaging-pipeline.$(RESET)" ; \
	$(ddev) exec "$(RUGGED_DEPLOY_PACKAGE_CMD)"

redeploy: redeploy-rugged-cli
	@for worker in $(RUGGED_WORKERS); do \
          echo -e "$(YELLOW)Redeploying code to $$worker.$(RESET)" ; \
          $(ddev) -s $$worker exec "$(RUGGED_DEPLOY_PACKAGE_CMD)" ; \
          echo -e "$(YELLOW)Restarting $$worker.$(RESET)" ; \
          $(ddev) $$worker-restart-worker; \
        done

clean-upstream:
	rm composer-integration php-tuf
fix-upstream: composer-integration php-tuf
composer-integration:
	ln -s d9-site/vendor/php-tuf/composer-integration .
	cd composer-integration; git remote set-url origin git@gitlab.com:drupal-infrastructure/package-signing/composer-tuf-integration.git
	cd composer-integration; git remote add github git@github.com:ergonlogic/composer-integration.git || /bin/true
php-tuf:
	ln -s d9-site/vendor/php-tuf/php-tuf .
	cd php-tuf; git remote set-url origin git@gitlab.com:drupal-infrastructure/package-signing/php-tuf-client.git
	cd php-tuf; git remote add github git@github.com:ergonlogic/php-tuf.git || /bin/true

lint:
	$(ddev) pipenv run flake8
	$(ddev) pipenv run bandit --severity-level all --confidence-level all --recursive rugged
	$(ddev) pyre
