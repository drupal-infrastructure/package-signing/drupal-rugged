.PHONY: tests tests-ci test-steps ci-local

tests: bin/behat #lint ##@rugged Run test suite
	$(make) tests-commands || $(make) tests-commands
	$(make) tests-general || $(make) tests-general
	$(make) tests-hashed-bins || $(make) tests-hashed-bins
	$(make) tests-scheduler || $(make) tests-scheduler
	$(make) tests-monitor-resilience || $(make) tests-monitor-resilience
	$(make) tests-refresh-expiry || $(make) tests-refresh-expiry
	$(make) tests-refresh-expiry-resilience || $(make) tests-refresh-expiry-resilience

tests-commands: bin/behat ##@rugged Run commands test suite
	$(make) disable-hashed-bins
	@$(behat) --stop-on-failure --profile=commands

tests-general: bin/behat ##@rugged Run general test suite
	$(make) disable-hashed-bins
	$(make) reset-after-scheduler-tests
	@$(behat) --stop-on-failure

# Testing of hashed bins requires config changes and a worker restarts. This
# cannot be done from within `behat` running on the packaging-pipeline (as the
# rest of the tests do.)
tests-hashed-bins: bin/behat ##@rugged Run hashed bins test suite
	$(make) enable-hashed-bins
	@$(behat) --stop-on-failure --profile=hashed-bins
	$(make) disable-hashed-bins

# Testing of the scheduler requires a config change and a worker restart. This
# cannot be done from within `behat` running on the packaging-pipeline (as the
# rest of the tests do.)
tests-scheduler: bin/behat ##@rugged Run scheduler test suite
	$(make) disable-hashed-bins
	$(make) prep-before-scheduler-tests
	@$(behat) --stop-on-failure --profile=scheduler
	$(make) reset-after-scheduler-tests

# Testing of the monitor worker's resilience to network instability requires
# several config changes and a worker restart. This cannot be done from within
# `behat` running on the packaging-pipeline (as the rest of the tests do.)
tests-monitor-resilience: bin/behat ##@rugged Run Monitor network instability resilience test suite
	$(make) disable-hashed-bins
	$(make) prep-before-monitor-resilience-tests
	@$(behat) --stop-on-failure --profile=monitor-resilience
	$(make) reset-after-monitor-tests

# Testing of the monitor worker's recurring refresh-expiry task.
tests-refresh-expiry: bin/behat ##@rugged Run Monitor refresh expiry test suite
	$(make) disable-hashed-bins
	$(make) prep-before-expiry-refresh-tests
	@$(behat) --stop-on-failure --profile=refresh-expiry
	$(make) reset-after-monitor-tests

tests-refresh-expiry-resilience: bin/behat ##@rugged Run Monitor refresh expiry resilience test suite
	$(make) disable-hashed-bins
	$(make) prep-before-expiry-refresh-resilience-tests
	@$(behat) --stop-on-failure --profile=refresh-expiry-resilience
	$(make) reset-after-monitor-tests

test-steps: bin/behat ##@rugged Print available test step definitions
	@$(behat) -dl

bin/behat:
	@$(composer) install

ci-local-tests: ci-local-tests-commands ci-local-tests-general ci-local-tests-hashed-bins ci-local-tests-monitor-resilience ci-local-tests-refresh-expiry ci-local-tests-scheduler ci-local-tests-refresh-expiry-resilience
ci-local-tests-commands: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-commands
ci-local-tests-general: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-general
ci-local-tests-hashed-bins: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-hashed-bins
ci-local-tests-monitor-resilience: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-monitor-resilience
ci-local-tests-refresh-expiry: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-refresh-expiry
ci-local-tests-refresh-expiry-resilience: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-refresh-expiry-resilience

ci-local-tests-scheduler: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests-scheduler

prep-before-scheduler-tests:
	$(ddev) monitor-worker-backup-config
	$(ddev) monitor-worker-enable-debug
	$(ddev) monitor-worker-extend-timeout
	$(ddev) monitor-worker-shorten-scan-period
	$(ddev) monitor-worker-shorten-reset-period
	$(ddev) monitor-worker-restart-worker
	$(ddev) extend-timeout

reset-after-scheduler-tests:
	$(ddev) monitor-worker-restore-default-config
	$(ddev) reset-timeout

prep-before-monitor-resilience-tests:
	$(ddev) monitor-worker-backup-config
	$(ddev) monitor-worker-enable-debug
	$(ddev) monitor-worker-enable-test-mode
	$(ddev) monitor-worker-shorten-timeout 1
	$(ddev) monitor-worker-shorten-scan-period
	$(ddev) monitor-worker-restart-worker

reset-after-monitor-tests:
	$(ddev) monitor-worker-restore-default-config

prep-before-expiry-refresh-tests:
	$(ddev) monitor-worker-backup-config
	$(ddev) monitor-worker-enable-debug
	$(ddev) monitor-worker-shorten-refresh-period
	$(ddev) monitor-worker-shorten-timeout 3
	$(ddev) monitor-worker-extend-scan-period
	$(ddev) monitor-worker-restart-worker

prep-before-expiry-refresh-resilience-tests:
	$(ddev) monitor-worker-backup-config
	$(ddev) monitor-worker-enable-debug
	$(ddev) monitor-worker-enable-test-mode
	$(ddev) monitor-worker-shorten-refresh-period
	$(ddev) monitor-worker-shorten-timeout 1
	$(ddev) monitor-worker-extend-scan-period
	$(ddev) monitor-worker-restart-worker
