# Start and destroy DDEV environments.

.PHONY: start stop off destroy

clean-docker:
	docker system prune -a
	docker volume prune

start: ##@rugged Start DDEV containers.
	@$(make-quiet) start-real
start-real:
	@$(ECHO) "$(YELLOW)Starting DDEV containers. (Be patient. This may take a while.)$(RESET)"
	ddev start $(QUIET)
	@$(ECHO) "$(LIME)RabbitMQ UI can be reached at https://rugged.ddev.site:15672"
	@$(ECHO) "$(LIME)Celery UI (flower) can be reached at https://rugged.ddev.site:8888"
	@$(ECHO) "$(YELLOW)Started DDEV containers.$(RESET)"

stop: ##@rugged Stop DDEV containers.
	@$(make-quiet) stop-real
stop-real:
	@$(ECHO) "$(YELLOW)Stopping DDEV containers.$(RESET)"
	ddev stop $(QUIET)
	@$(ECHO) "$(YELLOW)Stopped DDEV containers.$(RESET)"

pull-images: ##@rugged Pull latest container images
	@docker pull registry.gitlab.com/rugged/rugged/monitor-worker:latest
	@docker pull registry.gitlab.com/rugged/rugged/packaging-pipeline:latest
	@docker pull registry.gitlab.com/rugged/rugged/rabbitmq:latest
	@docker pull registry.gitlab.com/rugged/rugged/root-worker:latest
	@docker pull registry.gitlab.com/rugged/rugged/snapshot-worker:latest
	@docker pull registry.gitlab.com/rugged/rugged/targets-worker:latest
	@docker pull registry.gitlab.com/rugged/rugged/test-worker:latest
	@docker pull registry.gitlab.com/rugged/rugged/timestamp-worker:latest

off: ##@rugged Power off DDEV containers
	@$(make-quiet) off-real
off-real:
	@$(ECHO) "$(YELLOW)Powering off DDEV.$(RESET)"
	ddev poweroff $(QUIET)
	@$(ECHO) "$(YELLOW)DDEV powered off.$(RESET)"

destroy: ##@rugged Destroy DDEV containers
	@$(make-quiet) destroy-real
destroy-real:
	@$(ECHO) "$(YELLOW)Destroying DDEV containers.$(RESET)"
	ddev stop --remove-data $(QUIET)
	@$(ECHO) "$(YELLOW)Destroyed DDEV containers.$(RESET)"

restart: ##@rugged Rebuild DDEV containers
	@$(make-quiet) restart-real
restart-real:
	@$(ECHO) "$(YELLOW)Restarting DDEV containers.$(RESET)"
	ddev restart $(QUIET)
	@$(ECHO) "$(YELLOW)Restarted DDEV containers.$(RESET)"

delete: ##@rugged Delete DDEV project (and containers)
	@$(make-quiet) delete-real
delete-real:
	@$(ECHO) "$(YELLOW)Deleting DDEV project.$(RESET)"
	ddev delete $(QUIET)
	@$(ECHO) "$(YELLOW)Deleted DDEV project.$(RESET)"
