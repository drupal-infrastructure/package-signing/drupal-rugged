RUGGED_COMMAND_DOCS_DIR = docs/content/reference/commands

generate-docs: fixtures generate-api-docs generate-command-docs generate-feature-docs

generate-command-docs: clean-command-docs
	$(ddev_exec) make generate-command-index
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=generate-keys
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=generate-keys
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=initialize
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=initialize
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=add-targets
	$(ddev_exec) echo 'test' > fixtures/incoming_targets/example.txt
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=add-targets
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=config
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=config RUGGED_EXAMPLE_OPTIONS=\"\-\-worker=test-worker\"
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=echo
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=echo RUGGED_EXAMPLE_OPTIONS=\"\-\-worker=test-worker\"
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=logs
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=logs RUGGED_EXAMPLE_OPTIONS=\"\-\-worker=test-worker\"
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=status
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=status RUGGED_EXAMPLE_OPTIONS=\"\-\-worker=test-worker\"
	$(ddev_exec) make generate-command-doc RUGGED_COMMAND=remove-targets
	$(ddev_exec) make generate-command-example RUGGED_COMMAND=remove-targets RUGGED_EXAMPLE_OPTIONS=\"example.txt\"

clean-command-docs:
	rm -rf $(RUGGED_COMMAND_DOCS_DIR)/*
generate-command-index:
	@echo '---' > $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo 'title: Rugged commands' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo 'menuTitle: Commands' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo 'weight: 40' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo '---' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo 'Rugged CLI commands may be triggered by TUF repository administrators directly, or via the packaging pipeline. These will include:' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo '{{< children depth=3 showhidden=1 >}}' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo '#### Base command help text' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo '```terminal' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo '$$ rugged --help' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@$(rugged) " --help" >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md
	@echo '```' >> $(RUGGED_COMMAND_DOCS_DIR)/_index.md

generate-command-doc:
	@echo '---' > $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo 'title: "rugged $(RUGGED_COMMAND)"' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo 'hidden: True' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '---' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '#### Command help text' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '```terminal' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '$$ rugged $(RUGGED_COMMAND) --help' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@$(rugged) $(RUGGED_COMMAND) --help >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '```' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md

generate-command-example:
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '#### Command example output' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '```terminal' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '$$ rugged $(RUGGED_COMMAND) $(RUGGED_EXAMPLE_OPTIONS)' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@$(rugged) $(RUGGED_COMMAND) $(RUGGED_EXAMPLE_OPTIONS) &>> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md
	@echo '```' >> $(RUGGED_COMMAND_DOCS_DIR)/$(RUGGED_COMMAND).md

RUGGED_FEATURES_DOCS_DIR = docs/content/reference/features
RUGGED_FEATURES = $(shell cd features; find ./ -type f -name '*.feature')

generate-feature-docs: clean-feature-docs $(RUGGED_FEATURES)
clean-feature-docs:
	rm -rf $(RUGGED_FEATURES_DOCS_DIR)/*
	git checkout docs/content/reference/features/_index.md
$(RUGGED_FEATURES):
	$(ddev_exec) make generate-feature-doc RUGGED_FEATURE_FILE=features/$@ RUGGED_FEATURE_DOC_FILE=$(RUGGED_FEATURES_DOCS_DIR)/$(basename $@).md
generate-feature-doc:
	mkdir -p $(dir $(RUGGED_FEATURE_DOC_FILE))
	echo '---' > $(RUGGED_FEATURE_DOC_FILE)
	echo 'title: "$(shell cat $(RUGGED_FEATURE_FILE) | grep ^Feature)"' >> $(RUGGED_FEATURE_DOC_FILE)
	echo 'hidden: True' >> $(RUGGED_FEATURE_DOC_FILE)
	echo '---' >> $(RUGGED_FEATURE_DOC_FILE)
	echo >> $(RUGGED_FEATURE_DOC_FILE)
	echo '#### Test results for [`$(RUGGED_FEATURE_FILE)`](https://gitlab.com/rugged/rugged/-/blob/main/$(RUGGED_FEATURE_FILE))' >> $(RUGGED_FEATURE_DOC_FILE)
	echo 'Running `behat $(RUGGED_FEATURE_FILE)` results in:' >> $(RUGGED_FEATURE_DOC_FILE)
	echo >> $(RUGGED_FEATURE_DOC_FILE)
	echo '```gherkin' >> $(RUGGED_FEATURE_DOC_FILE)
	bin/behat --no-colors $(RUGGED_FEATURE_FILE) --profile=generate-docs | head -n -1 -- >> $(RUGGED_FEATURE_DOC_FILE)
	echo '```' >> $(RUGGED_FEATURE_DOC_FILE)

generate-api-docs: clean-api-docs
	$(ddev_exec) sudo sudo -u rugged pipenv run pdoc3 -o /tmp/rugged_api --html ./rugged
	$(ddev_exec) cp -r /tmp/rugged_api/rugged/* docs/content/reference/api/rugged/
clean-api-docs:
	$(ddev_exec) sudo rm -rf /tmp/rugged_api
	rm -rf docs/content/reference/api/rugged/*


