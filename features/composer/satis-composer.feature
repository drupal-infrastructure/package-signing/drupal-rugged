@satis @composer
Feature: Local Composer package repository
  In order to test the TUF implementation
  As a developer
  I need to ensure that Satis is installed and working

  Background:
    Given I run "make reset-satis"
      And I am in the "satis" directory

  Scenario: Satis is installed.
    Given I run "rm -rf satis/vendor"
      And I run "rm -rf satis/bin"
     When I run "composer install"
     Then I should get:
          """
          Installing dependencies from lock file
          """
     When I try to run "bin/satis --version"
     Then I should get:
          """
          Satis version 3.0.0-dev
          """

  Scenario: Build Satis repo.
     When I try to run "bin/satis build"
     Then I should not get:
          """
          File not found: ./satis.json
          Deprecation warning: Your package name
          is invalid, it should have a vendor name, a forward slash, and a package name.
          """
     Then I should get:
          """
          Scanning packages
          Writing packages.json
          Pruning include directories
          Writing web view
          """

  Scenario: Composer can access a Satis repo.
    Given I run "bin/satis build"
      And I am in the "../d9-site" directory
      And the file "composer.json" contains:
          """
          "url": "http://packages.ddev.site"
          """
     When I run "composer config --list | grep repositories"
     Then I should get:
          """
          http://packages.ddev.site
          """

  Scenario: Composer can install a package from a Satis repo.
    Given I run "bin/satis build"
      And I am in the "../d9-site" directory
      And I run "ln -sf satis-composer.json composer.json"
      And the file "composer.json" contains:
          """
          "url": "http://packages.ddev.site"
          """
      And I run "composer install"
     Then I should get:
          """
          - Locking drupal/token (1.11.0)
          Installing drupal/token (1.11.0): Extracting archive
          """
      And the following files should exist:
          """
          web/modules/contrib/token/token.info.yml
          """
