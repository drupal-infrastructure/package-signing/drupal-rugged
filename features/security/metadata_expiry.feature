@rugged @security @metadata-expiry
Feature: Configurable metadata expiry per role.
  In order to keep a TUF systems secure,
  As a TUF administrator
  I need to be able to configure each role's expiry separately.

  Background:
    Given I reset Rugged

  Scenario: Default metadata expiry is reasonable.
    Given I rebuild fixtures
     When I run the Rugged command "rugged status --local"
     Then I should get:
          """
          Role       Capability    Signatures      Version  TUF Spec    Expires
          ---------  ------------  ------------  ---------  ----------  ---------------------------------
          targets    Signing       1 / 1                 2  1.0.31      6 days, 23 hours
          snapshot   Signing       1 / 1                 2  1.0.31      6 days, 23 hours
          timestamp  Signing       1 / 1                 2  1.0.31      23 hours
          root       Signing       2 / 1                 1  1.0.31      364 days, 23 hours
          """

  Scenario: Roles can be configured to use arbitrary expiry periods.
    Given I run the Rugged command "rugged generate-keys"
      And I run "sudo cp features/fixtures/config/metadata_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged --debug initialize --local"
     When I run the Rugged command "rugged logs --local --limit=0"
     Then I should get:
          """
          DEBUG (repo.update_metadata_expiry): Setting 'targets' metadata expiry to 691200.
          DEBUG (repo.update_metadata_expiry): Setting 'snapshot' metadata expiry to 691200.
          DEBUG (repo.update_metadata_expiry): Setting 'timestamp' metadata expiry to 172800.
          DEBUG (repo.update_metadata_expiry): Setting 'root' metadata expiry to 31622400.
          """
     When I run the Rugged command "rugged status --local"
     Then I should get:
          """
          Role       Capability    Signatures      Version  TUF Spec    Expires
          ---------  ------------  ------------  ---------  ----------  -------------------------------
          targets    Signing       1 / 1                 1  1.0.31      7 days, 23 hours
          snapshot   Signing       1 / 1                 1  1.0.31      7 days, 23 hours
          timestamp  Signing       1 / 1                 1  1.0.31      1 day, 23 hours
          root       Signing       2 / 1                 1  1.0.31      1 year, 23 hours
          """

  Scenario Outline: Metadata expiry gets set correctly.
    Given I run "sudo cp features/fixtures/config/expiry/short_<ROLE>_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "<METAFILE>"
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the '<ROLE>' role is valid.
          """
      And I should not get:
          """
          error: Metadata for the '<ROLE>' role is not valid.
          """
    Given I wait "5" seconds
     When I try to run "sudo sudo -u rugged rugged validate"
     Then I should get:
          """
          error: ExpiredMetadataError thrown in validate_<ROLE>: <METAFILE> is expired
          error: Metadata for the '<ROLE>' role is not valid.
          """
      And I should not get:
          """
          Metadata for the '<ROLE>' role is valid.
          """
      And the expiry timestamp from "<METAFILE>" has not changed
    Examples:
      | ROLE      | METAFILE       |
      | root      | 1.root.json    |
      | snapshot  | snapshot.json  |
      | targets   | targets.json   |
      | timestamp | timestamp.json |
