@rugged @hsm_support @local @security @gitlab-160
Feature: Support for HSM-based root keys.
  In order to host TUF metadata using HSM-based root keys
  As an administrator
  I need to generate valid root metadata using keys and signatures from an HSM.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged mkdir -p /var/rugged/tuf_repo/tmp"
          # Ensure there's no cruft from previous test runs.
    Given the file "/var/rugged/tuf_repo/partial/1.root.json" does not exist
      And the file "/var/rugged/tuf_repo/partial/signable-1.root.json" does not exist

  Scenario: End-to-end initializing of a repo using both PEM and TUF keys.
          # Generate PEM keys; emulating an HSM.
    Given I run "sudo /usr/local/ssl/bin/openssl genpkey -algorithm ED25519 -out /var/rugged/tuf_repo/tmp/root_private.pem"
      And I run "sudo /usr/local/ssl/bin/openssl pkey -in /var/rugged/tuf_repo/tmp/root_private.pem -pubout | sudo tee -a /var/rugged/tuf_repo/tmp/root_public.pem"
      And I run "sudo /usr/local/ssl/bin/openssl genpkey -algorithm ED25519 -out /var/rugged/tuf_repo/tmp/root1_private.pem"
      And I run "sudo /usr/local/ssl/bin/openssl pkey -in /var/rugged/tuf_repo/tmp/root1_private.pem -pubout | sudo tee -a /var/rugged/tuf_repo/tmp/root1_public.pem"
      And I run "sudo chown rugged:rugged /var/rugged/tuf_repo/tmp/*.pem"
          # Generate online keys (TUF)
    Given I run the Rugged command "rugged generate-keys --local --role=snapshot"
      And I run the Rugged command "rugged generate-keys --local --role=targets"
      And I run the Rugged command "rugged generate-keys --local --role=timestamp"
          # Initialize a new partial root metadata.
    Given I run the Rugged command "rugged initialize-partial-root-metadata"
          # Add all verification keys
    Given I run the Rugged command "rugged add-verification-key root /var/rugged/tuf_repo/tmp/root_public.pem --key-type=pem"
      And I run the Rugged command "rugged add-verification-key root /var/rugged/tuf_repo/tmp/root1_public.pem --key-type=pem"
      And I run the Rugged command "rugged add-verification-key snapshot /var/rugged/verification_keys/snapshot/snapshot.pub"
      And I run the Rugged command "rugged add-verification-key targets /var/rugged/verification_keys/targets/targets.pub"
      And I run the Rugged command "rugged add-verification-key timestamp /var/rugged/verification_keys/timestamp/timestamp.pub"
          # Check that the contents of the partial root metadata conform to expectations.
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          root 2/2:
            type: ed25519
            keyid: 
          root 1/2:
            type: ed25519
            keyid: 
          snapshot 1/1:
            type: ed25519
            keyid: 
          targets 1/1:
            type: ed25519
            keyid: 
          timestamp 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """
      And I should not get:
          """
          signed by:
          """
          # Generate signatures; emulating an HSM.
    Given I run the Rugged command "/usr/local/ssl/bin/openssl pkeyutl -in /var/rugged/tuf_repo/partial/signable-1.root.json -rawin -sign -inkey /var/rugged/tuf_repo/tmp/root_private.pem | sudo sudo -u rugged tee -a /var/rugged/tuf_repo/tmp/root_signature.bin"
      And I run the Rugged command "/usr/local/ssl/bin/openssl pkeyutl -in /var/rugged/tuf_repo/partial/signable-1.root.json -rawin -sign -inkey /var/rugged/tuf_repo/tmp/root1_private.pem | sudo sudo -u rugged tee -a /var/rugged/tuf_repo/tmp/root1_signature.bin"
          # Add the signatures
    Given I run the Rugged command "rugged add-root-signature /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin --key-type=pem"
      And I run the Rugged command "rugged add-root-signature /var/rugged/tuf_repo/tmp/root1_public.pem /var/rugged/tuf_repo/tmp/root1_signature.bin --key-type=pem"
          # Check that the contents of the now-complete root metadata conform to expectations.
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 2
          Threshold: 1
          Signature 1 of 2: signed by root 1/2 -- VALID (keyid:
          Signature 2 of 2: signed by root 2/2 -- VALID (keyid:
          === KEYS ===
          root 1/2:
            type: ed25519
            keyid: 
          root 2/2:
            type: ed25519
            keyid: 
          snapshot 1/1:
            type: ed25519
            keyid: 
          targets 1/1:
            type: ed25519
            keyid: 
          timestamp 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """
          # Initialize a TUF repo using the generated root metadata.
    Given I run the Rugged command "mkdir -p /var/rugged/tuf_repo/metadata/"
      And I run the Rugged command "cp /var/rugged/tuf_repo/partial/1.root.json /var/rugged/tuf_repo/metadata/"
     When I run the Rugged command "rugged initialize --local"
     Then I should get:
          """
          Initializing new TUF repository at /var/rugged/tuf_repo.
          warning: No keys found for 'root'.
          warning: Initialized 'root' metadata from disk.
          If you did not intend to initialize with existing 'root' metadata then delete '1.root.json' and re-run this command.
          TUF repository initialized.
          """
          # Validate the resulting TUF repo metadata
     When I run the Rugged command "rugged validate metadata"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
      And I should not get:
          """
          failed to verify sig: Expected a byte of length 64
          """
