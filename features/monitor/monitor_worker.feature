@rugged @workers @monitor-worker
Feature: A worker to dispatch `add-targets` tasks based on monitoring a directory for target files.
  In order to allow a packaging pipeline not to hold credentials for the task queue,
  As a TUF administrator
  I need to ensure that a monitor worker can check a shared directory and dispatch `add-targets` tasks.

  Background:
    Given I reset Rugged

####
#
# To keep our local logs from filling up the disk, we default to a log-level of
# INFO. For these tests to pass, we need the log-level to be DEBUG. Also we
# default to a 5-second period between scans for new target files.
#
# To speed up development and testing that depends on the scheduler, we can
# reduce this period.
#
# To accomplish this in a local environment, run `make enable-debug-on-monitor`.
#
# To return to the default behaviour, run: `make disable-debug-on-monitor`.
#
# We've disabled the below tasks tagged with "@scheduler", since they won't
# work without the changes described above. To run these, run:
# `ddev behat --tags=scheduler`
#
####

  @scheduler
  Scenario: Ensure that monitor worker is polling.
    # We have to wait for 1 second to ensure that the scheduled task has run at least once.
    Given I wait "1" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          """
    Given I run the Rugged command "rugged logs --truncate --worker=monitor-worker"
      And I wait "1" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          """

  # @TODO: Check Celery worker log

  @scheduler
  Scenario: Notice when post-to-tuf directory has new files or directories.
    Given I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I wait "1" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          DEBUG (monitor-worker.find_new_targets_task): No new content found.
          """
      And I should not get:
          """
          INFO (monitor-worker.find_new_targets_task): New content found in post-to-tuf directory.
          INFO (monitor-worker.find_new_targets_task): Count of post-to-tuf content:
          INFO (monitor-worker.find_new_targets_task): Age of oldest post-to-tuf content:
          """
    Given I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
          # This uses sudo twice so that the resulting directory is owned by the 'rugged' user.
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I wait "1" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New content found in post-to-tuf directory.
          INFO (monitor-worker.find_new_targets_task): Count of post-to-tuf content: 1
          INFO (monitor-worker.find_new_targets_task): Age of oldest post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task): List of post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task):   tuf_tmp_TIMESTAMP
          INFO (monitor-worker.find_new_targets_task): No new targets found.
          """
      And I should not get:
          """
          INFO (monitor-worker.find_new_targets_task): No new content found.
          """

  @scheduler @tuf-ready-dir
  Scenario: Notice when a new set of targets is ready to add.
    Given I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
     When I run "sudo mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP /opt/post_to_tuf/tuf_ready_TIMESTAMP"
      And I wait "2" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          """

  @scheduler @tuf-ready-dir
  Scenario: Ensure that multiple `tuf_ready_` directories (in `post-to-tuf`) do not stop further targets being processed.
    Given I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/foo2.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I wait "1" seconds
     When I run "sudo mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP /opt/post_to_tuf/tuf_ready_TIMESTAMP"
      And I run "sudo mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP2 /opt/post_to_tuf/tuf_ready_TIMESTAMP2"
      And I wait "1" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP2
          """

  @scheduler @tuf-tmp-dir
  Scenario: Ensure that `tuf_tmp_` directory (in `post-to-tuf`) does not stop further targets being processed.
    Given I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/foo2.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I wait "1" seconds
     When I run "sudo mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP2 /opt/post_to_tuf/tuf_ready_TIMESTAMP2"
      And I wait "1" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New content found in post-to-tuf directory.
          DEBUG (monitor-worker.find_new_targets_task): List of post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task):   tuf_tmp_TIMESTAMP
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          """

  @scheduler @tuf-processing-dir
  Scenario: Ensure that `tuf_processing_` directory (in `post-to-tuf`) stops further targets being processed.
    Given I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_processing_TIMESTAMP/"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/"
      And I run "sudo fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/foo2.zip"
      And I run the Rugged command "rugged logs --truncate"
      And I wait "1" seconds
     When I run "sudo mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP2 /opt/post_to_tuf/tuf_ready_TIMESTAMP2"
      And I wait "1" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New content found in post-to-tuf directory.
          INFO (monitor-worker.find_new_targets_task): Count of post-to-tuf content: 2
          INFO (monitor-worker.find_new_targets_task): Age of oldest post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task): List of post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task):   tuf_processing_TIMESTAMP
          DEBUG (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP2
          INFO (monitor-worker.find_new_targets_task): Monitor worker is already processing targets. Waiting for next scan to continue.
          """
     Then I should not get:
          """
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for:
          """
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should not get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          """

  @scheduler @tuf-processing-dir @wip
  Scenario: Ensure that stale `tuf_processing_` directory (in `post-to-tuf`) is logged and ignored.

  @scheduler @tuf-processing-dir @wip
  Scenario: Ensure that stale `tuf_processing_` directory (in `inbound_targets`) is logged.
