@rugged @command @remove-targets @local
Feature: Command option to remove targets from the TUF repository locally.
  In order to benchmark TUF server performance
  As a Rugged developer
  I need to remove targets to a TUF repo locally.

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo
      And I run the Rugged command "rugged logs --truncate"
      And I am in the "/var/rugged/incoming_targets" directory

  Scenario: Fail when no target file to remove is specified.
     When I fail to run the Rugged command "rugged --debug remove-targets --local"
     Then I should not get:
          """
          Removed the following targets from the repository:
          """
      And I should get:
          """
          error: Missing argument '[TARGETS]...'.
          """

  Scenario: Fail when trying to remove a target file that is not in the repository.
    Given I try to run "grep -r test0.txt ../tuf_repo/metadata"
      And I should not get:
          """
          targets.json
          """
     When I fail to run the Rugged command "rugged --debug remove-targets test0.txt --local"
     Then I should not get:
          """
          Removed target 'test0.txt' from the 'targets' role.
          Removed the following targets from the repository:
          """
      And I should get:
          """
          error: Failed to remove one or more targets from TUF repository.
          Check the logs for more detailed error reporting.
          """

  Scenario: Remove a target file.
    Given file "test0.txt" contains "test0"
      And I run the Rugged command "rugged add-targets --local"
      And I run "grep -r test0.txt ../tuf_repo/metadata"
      And I should get:
          """
          targets.json
          """
     When I run the Rugged command "rugged --debug remove-targets test0.txt --local"
     Then I should get:
          """
          Removed the following targets from the repository:
          test0.txt
          """
     When I run the Rugged command "rugged logs --local --limit=0"
     Then I should get:
          """
          INFO (repo.remove_target): Removed target 'test0.txt' from the 'targets' role.
          INFO (repo._delete_removed_target): Deleted target file '/var/rugged/tuf_repo/targets/test0.txt'.
          DEBUG (repo.sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          INFO (repo.update_snapshot): Updated snapshot metadata.
          DEBUG (repo.sign_metadata): Signed 'snapshot' metadata with 'snapshot' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'snapshot' metadata to file '/var/rugged/tuf_repo/metadata/snapshot.json'.
          INFO (repo.update_timestamp): Updated timestamp metadata.
          DEBUG (repo.sign_metadata): Signed 'timestamp' metadata with 'timestamp' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'timestamp' metadata to file '/var/rugged/tuf_repo/metadata/timestamp.json'.
          """

  @gitlab-179
  Scenario: Remove a target file when Rugged is configure to delete targets after signing.
    Given I run "sudo cp /opt/rugged/features/fixtures/config/delete_targets.yaml /var/rugged/.config/rugged/config.yaml"
      And file "test0.txt" contains "test0"
      And I run the Rugged command "rugged --debug add-targets --local"
     When I run the Rugged command "rugged logs --local --limit=0"
     Then I should get:
          """
          DEBUG (repo._delete_target_after_signing): Deleted '/var/rugged/tuf_repo/targets/test0.txt' after signing.
          """
     When I try to run "ls -la /var/rugged/tuf_repo/targets/test0.txt"
     Then I should get:
          """
          ls: cannot access '/var/rugged/tuf_repo/targets/test0.txt': No such file or directory
          """
     When I run the Rugged command "rugged --debug remove-targets test0.txt --local"
     Then I should get:
          """
          Removed the following targets from the repository:
          test0.txt
          """
     When I run the Rugged command "rugged logs --local --limit=0"
     Then I should get:
          """
          INFO (repo.remove_target): Removed target 'test0.txt' from the 'targets' role.
          DEBUG (repo.sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          """
     And I should not get:
          """
          INFO (repo._delete_removed_target): Deleted target file '/var/rugged/tuf_repo/targets/test0.txt'.
          ERROR (logger.log_exception): FileNotFoundError thrown in _delete_removed_target: [Errno 2] No such file or directory: '/var/rugged/tuf_repo/targets/test0.txt'
          WARNING (repo._delete_removed_target): Failed to delete target file '/var/rugged/tuf_repo/targets/test0.txt'.
          """

  Scenario: Verify that metadata is updated.
    Given file "test0.txt" contains "test0"
      And I run the Rugged command "rugged add-targets --local"
      And I record a reference hash of "../tuf_repo/metadata/1.root.json"
      And the file "../tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "../tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "../tuf_repo/metadata/targets.json"
      And I record a reference hash of "../tuf_repo/metadata/timestamp.json"
     When I run the Rugged command "rugged remove-targets test0.txt --local"
     Then file "../tuf_repo/metadata/1.root.json" has not changed
      And the file "../tuf_repo/metadata/2.root.json" does not exist
      And file "../tuf_repo/metadata/snapshot.json" has changed
      And file "../tuf_repo/metadata/targets.json" has changed
      And file "../tuf_repo/metadata/timestamp.json" has changed

  Scenario: Remove multiple target files, including one in a subdirectory.
    Given file "test0.txt" contains "test0"
      And file "test1.txt" contains "test1"
      And the directory "foo/bar" exists
      And file "foo/bar/test2.txt" contains "test2"
      And I run the Rugged command "rugged add-targets --local"
     When I run the Rugged command "rugged --debug remove-targets test0.txt foo/bar/test2.txt --local"
     Then I should get:
          """
          Removed the following targets from the repository:
          test0.txt
          foo/bar/test2.txt
          Updated targets metadata.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     When I try to run "grep -r test0.txt ../tuf_repo/metadata"
     Then I should not get:
          """
          targets.json
          """
     When I run "grep -r test1.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I try to run "grep -r foo/bar/test2.txt ../tuf_repo/metadata"
     Then I should not get:
          """
          targets.json
          """
     When I run the Rugged command "rugged logs --local --limit=0"
     Then I should get:
          """
          INFO (repo.remove_target): Removed target 'test0.txt' from the 'targets' role.
          INFO (repo._delete_removed_target): Deleted target file '/var/rugged/tuf_repo/targets/test0.txt'.
          INFO (repo.remove_target): Removed target 'foo/bar/test2.txt' from the 'targets' role.
          INFO (repo._delete_removed_target): Deleted target file '/var/rugged/tuf_repo/targets/foo/bar/test2.txt'.
          DEBUG (repo._delete_empty_target_dirs): Cleaned up empty directory '/var/rugged/tuf_repo/targets/foo/bar'.
          DEBUG (repo._delete_empty_target_dirs): Cleaned up empty directory '/var/rugged/tuf_repo/targets/foo'.
          DEBUG (repo.sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          """
