@rugged @command @echo @credentials @test-worker
Feature: Script allows config file to specify credentials to connect to the queue worker.
  In order to securely communicate with workers
  As a Rugged administrator
  I need to configure scripts to run using specific credentials.

  Background:
    Given I reset Rugged

  Scenario: Send a ping/echo message, configuring a valid username.
    Given I run "sudo cp features/fixtures/config/valid_credentials.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged echo --worker=test-worker"
     Then I should get:
          """
          Sending test-worker Ping!...
          Done. Response was: test-worker PONG: Ping!
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """
     When I run "sudo rm -f /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged logs --worker=test-worker"
     Then I should get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, configuring an invalid username.
    Given I run "sudo cp features/fixtures/config/invalid_username.yaml /var/rugged/.config/rugged/config.yaml"
     When I fail to run the Rugged command "rugged --debug echo --worker=test-worker"
     Then I should get:
          """
          Sending test-worker Ping!...
          debug: Initializing connection to RabbitMQ.
          debug: RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          error: Failed to authenticate to worker queue. Check credentials.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (access_refused.__init__): RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          ERROR (task_queue.run_task): Failed to authenticate to worker queue. Check credentials.
          """
     When I run "sudo rm -f /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, configuring an invalid password.
    Given I run "sudo cp features/fixtures/config/invalid_password.yaml /var/rugged/.config/rugged/config.yaml"
     When I fail to run the Rugged command "rugged --debug echo --worker=test-worker"
     Then I should get:
          """
          Sending test-worker Ping!...
          debug: Initializing connection to RabbitMQ.
          debug: RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          error: Failed to authenticate to worker queue. Check credentials.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (access_refused.__init__): RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          ERROR (task_queue.run_task): Failed to authenticate to worker queue. Check credentials.
          """
     When I run "sudo rm -f /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, configuring an invalid host.
    Given I run "sudo cp features/fixtures/config/invalid_host.yaml /var/rugged/.config/rugged/config.yaml"
     When I fail to run the Rugged command "rugged --debug echo --worker=test-worker"
     Then I should get:
          """
          Sending test-worker Ping!...
          debug: Initializing connection to RabbitMQ.
          debug: RuggedConnectionError: Failed to connect to task queue.
          error: Failed to connect to worker queue. Check configuration.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (connection_error.__init__): RuggedConnectionError: Failed to connect to task queue.
          ERROR (task_queue.run_task): Failed to connect to worker queue. Check configuration.
          """
     When I run "sudo rm -f /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """
