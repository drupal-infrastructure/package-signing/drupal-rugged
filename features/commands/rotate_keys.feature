@rugged @security @rotate-keys
Feature: Rotate keys.
  In order to keep a TUF systems secure,
  As a TUF administrator
  I need to be able to rotate keys reliably.

  Background:
    Given I rebuild fixtures

  Scenario: The 'rotate-keys' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged rotate-keys --help"
     Then I should not get:
          """
          Error: No such command 'rotate-keys'.
          """
      And I should get:
          """
          Usage: rugged rotate-keys [OPTIONS]

            Rotate verification and signing keys.

          Options:
            --force
            --help   Show this message and exit.
          """

  Scenario: No change in keys results in a null op.
    Given I run "sudo cp features/fixtures/key_rotation/no_change.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/root/root1.pub"
      And I record a reference hash of "/var/rugged/verification_keys/timestamp/timestamp.pub"
      And I record a reference hash of "/var/rugged/verification_keys/snapshot/snapshot.pub"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
     When I run the Rugged command "rugged rotate-keys"
     Then I should get:
          """
          Preparing to rotate keys.
          """
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/root/root1.pub" has not changed
      And file "/var/rugged/verification_keys/timestamp/timestamp.pub" has not changed
      And file "/var/rugged/verification_keys/snapshot/snapshot.pub" has not changed
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          error: 'timestamp' metadata must contain hashes of 'snapshot.json'.
          debug: RuggedMetadataError: 'timestamp' metadata must contain hashes of 'snapshot.json'.
          error: Metadata for the 'timestamp' role is not valid.
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root1' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """

  Scenario: Remove any keys not in the current keys list (w/ confirmation prompt).
    Given I run "sudo cp features/fixtures/key_rotation/remove_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/timestamp/timestamp.pub"
      And I record a reference hash of "/var/rugged/verification_keys/snapshot/snapshot.pub"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And the file "/var/rugged/verification_keys/root/root1.pub" exists
      And the file "/var/rugged/signing_keys/root/root1" exists
     When I run the Rugged command "rugged --debug rotate-keys --force"
     Then I should get:
          """
          Preparing to rotate keys.
          The key 'root1' for the 'root' role will be removed. Confirm? [y/N]: y
          debug: Removing 'root1' key from 'root' role in root metadata.
          Removed 'root1' key from the 'root' role.
          """
      And the file "/var/rugged/verification_keys/root/root1.pub" does not exist
      And the file "/var/rugged/signing_keys/root/root1" does not exist
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/timestamp/timestamp.pub" has not changed
      And file "/var/rugged/verification_keys/snapshot/snapshot.pub" has not changed
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
      # Removing a 'root' key only requires regenerating root metadata.
      # However, since root is always versioned, we get a new file.
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" exists
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          debug: Root metadata contains the 'root1' key for the 'root' role.
          error: Root metadata contains unexpected key with ID:
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """

  Scenario: Generate any keys missing from the current list.
    Given I run "sudo cp features/fixtures/key_rotation/add_new_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/root/root1.pub"
      And I record a reference hash of "/var/rugged/verification_keys/timestamp/timestamp.pub"
      And I record a reference hash of "/var/rugged/verification_keys/snapshot/snapshot.pub"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And the file "/var/rugged/verification_keys/root/root2.pub" does not exist
      And the file "/var/rugged/signing_keys/root/root2" does not exist
     When I run the Rugged command "rugged --debug rotate-keys --force"
     Then I should get:
          """
          Preparing to rotate keys.
          debug: Adding 'root2' key to 'root' role in root metadata.
          debug: Loaded 'root2' signing key for 'root' role.
          Added 'root2' key to the 'root' role.
          """
      And the file "/var/rugged/verification_keys/root/root2.pub" exists
      And the file "/var/rugged/signing_keys/root/root2" exists
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/root/root1.pub" has not changed
      And file "/var/rugged/verification_keys/timestamp/timestamp.pub" has not changed
      And file "/var/rugged/verification_keys/snapshot/snapshot.pub" has not changed
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
      # Removing a 'root' key only requires regenerating root metadata.
      # However, since root is always versioned, we get a new file.
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" exists
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root1' key for the 'root' role.
          debug: Root metadata contains the 'root2' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """

  Scenario: Rotate one of two root keys, with a threshold of 1.
    Given I run "sudo cp features/fixtures/key_rotation/replace_root.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/timestamp/timestamp.pub"
      And I record a reference hash of "/var/rugged/verification_keys/snapshot/snapshot.pub"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And the file "/var/rugged/verification_keys/root/root1.pub" exists
      And the file "/var/rugged/verification_keys/root/root2.pub" does not exist
     When I run the Rugged command "rugged --debug rotate-keys --force"
     Then I should get:
          """
          Preparing to rotate keys.
          debug: Adding 'root2' key to 'root' role in root metadata.
          debug: Loaded 'root2' signing key for 'root' role.
          debug: Removing 'root1' key from 'root' role in root metadata.
          Added 'root2' key to the 'root' role.
          Removed 'root1' key from the 'root' role.
          """
      And the file "/var/rugged/verification_keys/root/root1.pub" does not exist
      And the file "/var/rugged/verification_keys/root/root2.pub" exists
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/timestamp/timestamp.pub" has not changed
      And file "/var/rugged/verification_keys/snapshot/snapshot.pub" has not changed
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
      # Removing a 'root' key only requires regenerating root metadata.
      # However, since root is always versioned, we get a new file.
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" exists
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          debug: Root metadata contains the 'root1' key for the 'root' role.
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root2' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """

  @wip
  Scenario: Rotate one of two root keys, with a threshold of 2.
# @TODO: We may need to special-case rotating root keys, because the root role
#        needs to be signed by a threshold of new and old keys. This provides
#        continuity from the initial root metadata through to whatever version
#        is current.
#        As it stands, this can be worked around by running `rugged rotate-keys`
#        twice. First, add the new root keys. Then, remove the old ones.

  Scenario: Rotate timestamp key.
    Given I run "sudo cp features/fixtures/key_rotation/replace_timestamp.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/root/root1.pub"
      And I record a reference hash of "/var/rugged/verification_keys/snapshot/snapshot.pub"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And the file "/var/rugged/verification_keys/timestamp/timestamp.pub" exists
      And the file "/var/rugged/verification_keys/timestamp/timestamp1.pub" does not exist
     When I run the Rugged command "rugged --debug rotate-keys --force"
     Then I should get:
          """
          Preparing to rotate keys.
          debug: Adding 'timestamp1' key to 'timestamp' role in root metadata.
          debug: Loaded 'timestamp1' signing key for 'timestamp' role.
          debug: Removing 'timestamp' key from 'timestamp' role in root metadata.
          Added 'timestamp1' key to the 'timestamp' role.
          Removed 'timestamp' key from the 'timestamp' role.
          """
      And the file "/var/rugged/verification_keys/timestamp/timestamp.pub" does not exist
      And the file "/var/rugged/verification_keys/timestamp/timestamp1.pub" exists
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/root/root1.pub" has not changed
      And file "/var/rugged/verification_keys/snapshot/snapshot.pub" has not changed
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
      # Rotating 'timestamp' key requires regenerating root and timestamp metadata.
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" exists
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root1' key for the 'root' role.
          debug: Root metadata contains the 'timestamp1' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """

  Scenario: Rotate snapshot key.
    Given I run "sudo cp features/fixtures/key_rotation/replace_snapshot.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/root/root1.pub"
      And I record a reference hash of "/var/rugged/verification_keys/timestamp/timestamp.pub"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And the file "/var/rugged/verification_keys/snapshot/snapshot.pub" exists
      And the file "/var/rugged/verification_keys/snapshot/snapshot1.pub" does not exist
     When I run the Rugged command "rugged --debug rotate-keys --force"
     Then I should get:
          """
          Preparing to rotate keys.
          debug: Adding 'snapshot1' key to 'snapshot' role in root metadata.
          debug: Loaded 'snapshot1' signing key for 'snapshot' role.
          debug: Removing 'snapshot' key from 'snapshot' role in root metadata.
          Added 'snapshot1' key to the 'snapshot' role.
          Removed 'snapshot' key from the 'snapshot' role.
          """
      And the file "/var/rugged/verification_keys/snapshot/snapshot.pub" does not exist
      And the file "/var/rugged/verification_keys/snapshot/snapshot1.pub" exists
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/root/root1.pub" has not changed
      And file "/var/rugged/verification_keys/timestamp/timestamp.pub" has not changed
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
      # Rotating a 'snapshot' key requires regenerating root, timestamp and snapshot metadata.
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" exists
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root1' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot1' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """

  Scenario: Rotate targets key.
    Given I run "sudo cp features/fixtures/key_rotation/replace_targets.yaml /var/rugged/.config/rugged/config.yaml"
      And I record a reference hash of "/var/rugged/verification_keys/root/root.pub"
      And I record a reference hash of "/var/rugged/verification_keys/root/root1.pub"
      And I record a reference hash of "/var/rugged/verification_keys/timestamp/timestamp.pub"
      And I record a reference hash of "/var/rugged/verification_keys/snapshot/snapshot.pub"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
      And the file "/var/rugged/verification_keys/targets/targets.pub" exists
      And the file "/var/rugged/verification_keys/targets/targets1.pub" does not exist
     When I run the Rugged command "rugged --debug rotate-keys --force"
     Then I should get:
          """
          Preparing to rotate keys.
          debug: Adding 'targets1' key to 'targets' role in root metadata.
          debug: Loaded 'targets1' signing key for 'targets' role.
          debug: Removing 'targets' key from 'targets' role in root metadata.
          Added 'targets1' key to the 'targets' role.
          Removed 'targets' key from the 'targets' role.
          """
      And the file "/var/rugged/verification_keys/targets/targets.pub" does not exist
      And the file "/var/rugged/verification_keys/targets/targets1.pub" exists
      And file "/var/rugged/verification_keys/root/root.pub" has not changed
      And file "/var/rugged/verification_keys/root/root1.pub" has not changed
      And file "/var/rugged/verification_keys/timestamp/timestamp.pub" has not changed
      And file "/var/rugged/verification_keys/snapshot/snapshot.pub" has not changed
      # Rotating a 'targets' key requires regenerating all role metadata.
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" exists
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has changed
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
          """
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          debug: RuggedKeyError:
          """
      And I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root1' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets1' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """
