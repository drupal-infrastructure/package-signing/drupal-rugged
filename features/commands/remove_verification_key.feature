@rugged @command @remove-verification-key @local @gitlab-163
Feature: Command to remove a verification key from partial root metadata.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to remove verification keys from partial root metadata.

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo
      And I run the Rugged command "rugged initialize-partial-root-metadata-for-update"

  Scenario: The 'remove-verification-key' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged remove-verification-key --help"
     Then I should not get:
          """
          Error: No such command 'remove-verification-key'.
          """
     Then I should get:
          """
          Usage: rugged remove-verification-key [OPTIONS] KEYID

             Remove a verification key from partial Root metadata.

           Options:
             -y, --yes  Assume 'yes' to confirmation prompt.
             --help     Show this message and exit.
          """

  Scenario: Removing a verification key fails without existing partial root metadata.
    Given I run the Rugged command "rm /var/rugged/tuf_repo/partial/2.root.json"
     When I fail to run the Rugged command "rugged remove-verification-key intentionally_invalid"
     Then I should get:
          """
          error: FileNotFoundError thrown in remove_verification_key_cmd: Can't open /var/rugged/tuf_repo/partial/1.root.json
          error: A metadata file was not found at the expected path: /var/rugged/tuf_repo/partial/1.root.json
          """

  Scenario: Removing a verification key fails if the key is not in partial metadata.
     When I fail to run the Rugged command "rugged --debug remove-verification-key intentionally-invalid"
     Then I should get:
          """
          error: The provided key ID (intentionally-invalid) was not found in partial root metadata.
          """

  Scenario: The remove-verification-key Rugged command will prompt before removing a key (and has a '--yes' option).
     # N.B. Here's we're using 'jq' (in a sub-shell) to look up a keyid from
     # the partial root metadata. Pretty neat, huh?
     When I fail to run the Rugged command "rugged --debug remove-verification-key `cat /var/rugged/tuf_repo/partial/2.root.json | jq .signed.keys | jq 'keys' | jq 'nth(0)' -r`"
     Then I should get:
          """
          Finding role to which the provided key
          is assigned.
          Remove key with ID
          [y/N]:
          Aborted!
          """
     Then I should not get:
          """
          Proceeding to remove key with ID
          """
     When I run the Rugged command "rugged --debug remove-verification-key --yes `cat /var/rugged/tuf_repo/partial/2.root.json | jq .signed.keys | jq 'keys' | jq 'nth(0)' -r`"
     Then I should get:
          """
          Finding role to which the provided key
          is assigned.
          --yes option provided. Bypassing key-removal confirmation prompt.
          Proceeding to remove key with ID
          """

  Scenario: Remove TUF key from partial root metadata.
    Given I record a reference hash of "/var/rugged/tuf_repo/partial/2.root.json"
      And I record a reference hash of "/var/rugged/tuf_repo/partial/signable-2.root.json"
     When I run the Rugged command "rugged --debug remove-verification-key --yes `cat /var/rugged/tuf_repo/partial/2.root.json | jq .signed.roles.root.keyids | jq 'nth(0)' -r`"
     Then I should get:
          """
          Proceeding to remove key with ID
          Removing key
          from list of keys in partial Root metadata.
          Removed key
          Updating partial metadata expiry.
          Updated partial root metadata at: /var/rugged/tuf_repo/partial/2.root.json
          """
      And file "/var/rugged/tuf_repo/partial/2.root.json" has changed
      And file "/var/rugged/tuf_repo/partial/signable-2.root.json" has changed
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 2 (2.root.json).
          === KEYS ===
          root 1/1:
            type: ed25519
            keyids: 
          snapshot 1/1:
            type: ed25519
            keyids: 
          targets 1/1:
            type: ed25519
            keyids: 
          timestamp 1/1:
            type: ed25519
            keyids: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """

  @wip
  Scenario: Removing a verification key fails if it would make it impossible to achieve a threshold of signatures.

  Scenario: End-to-end initializing a repo, then updating it using both PEM and TUF keys.
    Given I reset Rugged
      And I run "sudo rm -rf /var/rugged/tuf_repo/tmp"
      And I run the Rugged command "mkdir -p /var/rugged/tuf_repo/tmp"
      And I generate a complete set of Rugged keys
      And I generate complete partial root metadata
      And I generate signatures of partial root metadata
      And I run the Rugged command "rugged add-root-signature /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin --key-type=pem"
      And I run the Rugged command "rugged add-root-signature /var/rugged/tuf_repo/tmp/root1_public.pem /var/rugged/tuf_repo/tmp/root1_signature.bin --key-type=pem"
      And I run the Rugged command "mkdir -p /var/rugged/tuf_repo/metadata/"
      And I run the Rugged command "cp /var/rugged/tuf_repo/partial/1.root.json /var/rugged/tuf_repo/metadata/"
      And I run the Rugged command "rugged initialize --local"
     When I run the Rugged command "rugged validate metadata"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
    Given I run the Rugged command "rugged initialize-partial-root-metadata-for-update"
          # Remove both root keys, since we don't know which one will be removed first.
      And I run the Rugged command "rugged --debug remove-verification-key --yes `cat /var/rugged/tuf_repo/partial/2.root.json | jq .signed.roles.root.keyids | jq 'nth(0)' -r`"
      And I run the Rugged command "rugged --debug remove-verification-key --yes `cat /var/rugged/tuf_repo/partial/2.root.json | jq .signed.roles.root.keyids | jq 'nth(0)' -r`"
          # Generate a third key.
      And I run the Rugged command "/usr/local/ssl/bin/openssl genpkey -algorithm ED25519 -out /var/rugged/tuf_repo/tmp/root2_private.pem"
      And I run the Rugged command "/usr/local/ssl/bin/openssl pkey -in /var/rugged/tuf_repo/tmp/root2_private.pem -pubout | sudo tee -a /var/rugged/tuf_repo/tmp/root2_public.pem"
          # Add back the first and third keys.
      And I run the Rugged command "rugged add-verification-key root /var/rugged/tuf_repo/tmp/root_public.pem --key-type=pem"
      And I run the Rugged command "rugged add-verification-key root /var/rugged/tuf_repo/tmp/root2_public.pem --key-type=pem"
          # Generate new signatures with the new set of keys.
      And I run the Rugged command "/usr/local/ssl/bin/openssl pkeyutl -in /var/rugged/tuf_repo/partial/signable-2.root.json -rawin -sign -inkey /var/rugged/tuf_repo/tmp/root_private.pem -out /var/rugged/tuf_repo/tmp/new_root_signature.bin"
      And I run the Rugged command "/usr/local/ssl/bin/openssl pkeyutl -in /var/rugged/tuf_repo/partial/signable-2.root.json -rawin -sign -inkey /var/rugged/tuf_repo/tmp/root2_private.pem -out /var/rugged/tuf_repo/tmp/new_root2_signature.bin"
          # Add the new signatures.
      And I run the Rugged command "rugged add-root-signature /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/new_root_signature.bin --key-type=pem"
      And I run the Rugged command "rugged add-root-signature /var/rugged/tuf_repo/tmp/root2_public.pem /var/rugged/tuf_repo/tmp/new_root2_signature.bin --key-type=pem"
          # Deploy the new Root metadata.
     When I run the Rugged command "cp /var/rugged/tuf_repo/partial/2.root.json /var/rugged/tuf_repo/metadata/"
      And I run the Rugged command "rugged validate metadata"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
