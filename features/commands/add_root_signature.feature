@rugged @command @local @add-root-signature @gitlab-160 @gitlab-172
Feature: Command to add signatures to partial root metadata.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to add signatures to partial root metadata.

  Background:
    Given I reset Rugged
      And I run the Rugged command "mkdir -p /var/rugged/tuf_repo/partial /var/rugged/tuf_repo/tmp"
      And I generate a complete set of Rugged keys
      And I generate complete partial root metadata
      And I generate signatures of partial root metadata

  Scenario: The 'add-root-signature' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged add-root-signature --help"
     Then I should not get:
          """
          Error: No such command 'add-root-signature'.
          """
     Then I should get:
          """
          Usage: rugged add-root-signature [OPTIONS] PATH_TO_VERIFICATION_KEY
                                       PATH_TO_SIGNATURE

            Add a signature to partial Root metadata.

          Options:
            --key-type TEXT  The type of key used to generate the signature ('tuf' or
                             'pem', default 'tuf').
            --help           Show this message and exit.
          """

  Scenario: Provided verification key does not match a root key.
     When I fail to run the Rugged command "rugged --debug add-root-signature intentionally-invalid /var/rugged/tuf_repo/tmp/root_signature.bin"
     Then I should get:
          """
          Usage: rugged add-root-signature [OPTIONS] PATH_TO_VERIFICATION_KEY
                                           PATH_TO_SIGNATURE
          Try 'rugged add-root-signature --help' for help.
          Error: Invalid value for 'PATH_TO_VERIFICATION_KEY': Path 'intentionally-invalid' does not exist.
          """
     And I should not get:
          """
          debug: The provided verification key was found among root keys:
          """

  Scenario: Signature file is missing.
     When I fail to run the Rugged command "rugged --debug add-root-signature --key-type=pem /var/rugged/tuf_repo/tmp/root_public.pem intentionally-invalid"
     Then I should get:
          """
          Usage: rugged add-root-signature [OPTIONS] PATH_TO_VERIFICATION_KEY
                                           PATH_TO_SIGNATURE
          Try 'rugged add-root-signature --help' for help.
          Error: Invalid value for 'PATH_TO_SIGNATURE': Path 'intentionally-invalid' does not exist.
          """

  Scenario: Partial Root metadata is missing.
    Given I run the Rugged command "rm /var/rugged/tuf_repo/partial/1.root.json"
     When I fail to run the Rugged command "rugged --debug add-root-signature --key-type=pem /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin"
     Then I should get:
          """
          error: A metadata file was not found at the expected path: /var/rugged/tuf_repo/partial/1.root.json
          """
     And I should not get:
          """
          debug: Checking for expected metadata file at: /var/rugged/tuf_repo/partial/1.root.json.
          debug: Loading partial metadata file from: /var/rugged/tuf_repo/partial/1.root.json.
          debug: Loaded metadata for 'root' role from '/var/rugged/tuf_repo/partial/1.root.json'.
          """

  Scenario: Add signature to root metadata.
    Given I record a reference hash of "/var/rugged/tuf_repo/partial/1.root.json"
    # Add the signatures
     When I run the Rugged command "rugged --debug add-root-signature /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin --key-type=pem"
     Then I should get:
          """
          debug: Loaded metadata for 'root' role from '/var/rugged/tuf_repo/partial/1.root.json'.
          debug: The provided verification key was found among root keys: /var/rugged/tuf_repo/tmp/root_public.pem
          debug: Loading signature from file: /var/rugged/tuf_repo/tmp/root_signature.bin
          debug: Loaded signature from file: /var/rugged/tuf_repo/tmp/root_signature.bin
          debug: Wrote 'root' metadata to file '/var/rugged/tuf_repo/partial/1.root.json'.
          """
      And file "/var/rugged/tuf_repo/partial/1.root.json" has changed
      And I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === SIGNATURES ===
          Signatures: 1
          Threshold: 1
          Signature 1 of 1: signed by root 1/2 -- VALID (keyid:
          """

  Scenario: Add multiple signatures to root metadata.
    Given I run the Rugged command "rugged --debug add-root-signature /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin --key-type=pem"
      And I run the Rugged command "rugged --debug add-root-signature /var/rugged/tuf_repo/tmp/root1_public.pem /var/rugged/tuf_repo/tmp/root1_signature.bin --key-type=pem"
      And I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === SIGNATURES ===
          Signatures: 2
          Threshold: 1
          Signature 1 of 2: signed by root 1/2 -- VALID (keyid:
          Signature 2 of 2: signed by root 2/2 -- VALID (keyid:
          === KEYS ===
          root 1/2:
            type: ed25519
            keyids: 
          root 2/2:
            type: ed25519
            keyids: 
          snapshot 1/1:
            type: ed25519
            keyids: 
          targets 1/1:
            type: ed25519
            keyids: 
          timestamp 1/1:
            type: ed25519
            keyids: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """

  Scenario: Confirm invalid signatures can't be added
    Given I fail to run the Rugged command "rugged --debug add-root-signature --key-type=pem /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root1_signature.bin"
     Then I should get:
          """
          error: The provided signature
          is invalid for the
          """
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === SIGNATURES ===
          Signatures: 0
          """

  Scenario: Validate TUF repo initialized from partial metadata and added signatures.
    Given I run the Rugged command "mkdir -p /var/rugged/tuf_repo/metadata"
      And I run the Rugged command "cp /var/rugged/tuf_repo/partial/1.root.json /var/rugged/tuf_repo/metadata/"
          # Initialize the repo
    Given I run the Rugged command "rugged --debug add-root-signature /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin --key-type=pem"
      And I run the Rugged command "rugged --debug add-root-signature /var/rugged/tuf_repo/tmp/root1_public.pem /var/rugged/tuf_repo/tmp/root1_signature.bin --key-type=pem"
     When I run the Rugged command "rugged --debug initialize --local"
          # Check for expected output.
      And I should get:
          """
          debug: Loaded metadata for 'root' role from '/var/rugged/tuf_repo/metadata/1.root.json'.
          TUF repository initialized.
          """
          # Validate that the repo is valid
     When I run the Rugged command "rugged validate metadata"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
