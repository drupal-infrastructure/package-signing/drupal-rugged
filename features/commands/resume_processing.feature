@rugged @command @resume-processing @gitlab-183
Feature: Command to resume processing of scheduled tasks.
  In order to cleanly run certain operations
  As an administrator
  I need to resume processing of scheduled tasks on the monitor-worker.

  Background:
    Given I reset Rugged
      And the "/opt/post_to_tuf/tuf_paused" directory should not exist

  Scenario: The 'resume-processing' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged resume-processing --help"
     Then I should not get:
          """
          Error: No such command 'resume-processing'.
          """
     Then I should get:
          """
          Usage: rugged resume-processing [OPTIONS]

            Resume processing of scheduled tasks on the monitor-worker.

          Options:
            --help  Show this message and exit.
          """

  Scenario: The resume-processing Rugged command warns if there is no semaphore for it to delete.
     When I run the Rugged command "rugged resume-processing"
     Then I should get:
          """
          Resuming processing of scheduled tasks.
          warning: No pause-processing flag was found at: /opt/post_to_tuf/tuf_paused
          """

  Scenario: The resume-processing Rugged command deletes the relevant semaphore.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_paused"
     When I run the Rugged command "rugged --debug resume-processing"
     Then I should get:
          """
          Resuming processing of scheduled tasks.
          debug: Detected pause-processing flag at: /opt/post_to_tuf/tuf_paused
          debug: Deleting 'pause-processing' semaphore directory: /opt/post_to_tuf/tuf_paused
          Resumed processing of scheduled tasks.
          """
