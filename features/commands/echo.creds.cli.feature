@rugged @command @echo @credentials @test-worker
Feature: Script allows specifying credentials to connect to the queue worker.
  In order to securely communicate with workers
  As a Rugged administrator
  I need to run scripts using specific credentials.

  Background:
    Given I reset Rugged

  Scenario: Send a ping/echo message, specifying valid credentials.
     When I run the Rugged command "rugged echo --worker=test-worker --broker-connection-string='pyamqp://rugged-rabbitmq:badpassword@rabbitmq//'"
     Then I should get:
          """
          Sending test-worker Ping!...
          Done. Response was: test-worker PONG: Ping!
          """
     Then I should not get:
          """
          debug: Initializing connection to RabbitMQ.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """
     Then I should not get:
          """
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          """
     When I run the Rugged command "rugged logs --worker=test-worker"
     Then I should get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, specifying an invalid username.
     When I fail to run the Rugged command "rugged --debug echo --worker=test-worker --broker-connection-string='pyamqp://intentionally-invalid:badpassword@rabbitmq//'"
     Then I should get:
          """
          Sending test-worker Ping!...
          debug: Initializing connection to RabbitMQ.
          debug: RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          error: Failed to authenticate to worker queue. Check credentials.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (access_refused.__init__): RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          ERROR (task_queue.run_task): Failed to authenticate to worker queue. Check credentials.
          """
     When I run the Rugged command "rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, specifying an invalid password.
     When I fail to run the Rugged command "rugged --debug echo --worker=test-worker --broker-connection-string='pyamqp://rugged-rabbitmq:intentionally-invalid@rabbitmq//'"
     Then I should get:
          """
          Sending test-worker Ping!...
          debug: Initializing connection to RabbitMQ.
          debug: RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          error: Failed to authenticate to worker queue. Check credentials.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (access_refused.__init__): RuggedAccessRefused: Failed to authenticate to RabbitMQ.
          ERROR (task_queue.run_task): Failed to authenticate to worker queue. Check credentials.
          """
     When I run the Rugged command "rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  @wip
  Scenario: Ensure --timeout option works.

  Scenario: Send a ping/echo message, specifying an invalid host.
     When I fail to run the Rugged command "rugged --debug echo --worker=test-worker --broker-connection-string='pyamqp://rugged-rabbitmq:badpassword@intentionally-invalid//'"
     Then I should get:
          """
          Sending test-worker Ping!...
          debug: Initializing connection to RabbitMQ.
          debug: RuggedConnectionError: Failed to connect to task queue.
          error: Failed to connect to worker queue. Check configuration.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (connection_error.__init__): RuggedConnectionError: Failed to connect to task queue.
          ERROR (task_queue.run_task): Failed to connect to worker queue. Check configuration.
          """
     When I run the Rugged command "rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """
