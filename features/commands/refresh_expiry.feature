@rugged @command @refresh-expiry @gitlab-155
Feature: Command to refresh metadata expiry periods.
  In order to keep a TUF systems operating,
  As a TUF administrator
  I need to ensure that metadata expiry periods can be refreshed.

# @TODO: Add scenario where no metadate requires a refresh.
# @TODO: Add scenario where we force a refresh (`--force`).

  Background:
    Given I reset Rugged

  Scenario: The 'refresh-expiry' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged refresh-expiry --help"
     Then I should not get:
          """
          Error: No such command 'refresh-expiry'.
          """
      And I should get:
          """
          Usage: rugged refresh-expiry [OPTIONS]

            Refresh expiry period for any metadata where it will expire imminently.

          Options:
            --force  Force refresh of all metadata expiry periods.
            --help   Show this message and exit.
          """

  Scenario Outline: Metadata expiry gets refreshed.
    Given I run "sudo cp features/fixtures/config/expiry/short_<ROLE>_expiry.yaml /var/rugged/.config/rugged/config.yaml"
     # FYI: We're initializing the repo locally here, so that it will pick up
     #      our local config (deployed in the previous step). Otherwise, we
     #      would have to add additional Behat profiles, and related tooling,
     #      to deploy the config to workers, and then restart them. This cannot
     #      happen from within a running test, since (by design) we don't have
     #      access to restarting services on the workers.
      And I initialize a Rugged repo
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/<ROLE>.json"
      And I record the expiry timestamp from "<ROLE>.json"
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the '<ROLE>' role is valid.
          """
          #This is the short expiry that we set in the config above.
    Given I wait "5" seconds
     When I try to run "sudo sudo -u rugged rugged validate"
     Then I should get:
          """
          error: ExpiredMetadataError thrown in validate_<ROLE>: <ROLE>.json is expired
          error: Metadata for the '<ROLE>' role is not valid.
          """
      And the expiry timestamp from "<ROLE>.json" has not changed
     When I run the Rugged command "rugged --debug refresh-expiry"
     Then I should get:
          """
          Preparing to refresh metadata expiry periods.
          Fetching expiring metadata from workers.
          debug: Fetching expiring metadata from targets-worker.
          debug: Fetched expiring metadata from targets-worker.
          debug: Fetching expiring metadata from snapshot-worker.
          debug: Fetched expiring metadata from snapshot-worker.
          debug: Fetching expiring metadata from timestamp-worker.
          debug: Fetched expiring metadata from timestamp-worker.
          Expiring metadata from targets-worker:
          Expiring metadata from snapshot-worker:
          Expiring metadata from timestamp-worker:
            <ROLE>.json
          Refreshed <ROLE> metadata expiry period.
          """
     When I run the Rugged command "rugged logs --worker=<ROLE>-worker --limit=0"
     Then I should get:
          """
          '<ROLE>.json' metadata expiry is imminent
          """
     # FYI: While the metadata expiry period has been extended, it will use the
     #      config on the relevant worker, instead of the one we used to
     #      initialize the repo. This should be fine, since all we're concerned
     #      about here is forcing a quick expiration of the metadata, so that
     #      we can show that's it has been refreshed.
     When I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/<ROLE>.json" to change
     Then the expiry timestamp from "<ROLE>.json" has changed
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the '<ROLE>' role is valid.
          """
    Examples:
      | ROLE |
      | targets   |
      | snapshot  |
      | timestamp |

  Scenario: Refreshing targets metadata updates snapshot and timestamp.
    Given I run "sudo cp features/fixtures/config/expiry/short_targets_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "targets.json"
      And I record the expiry timestamp from "snapshot.json"
      And I record the expiry timestamp from "timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
     When I run the Rugged command "rugged --debug refresh-expiry"
     Then I should get:
          """
          Dispatching task to refresh expiring metadata on targets-worker.
          Refreshed targets metadata expiry period.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     When I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/timestamp.json" to change
      And I wait "5" seconds
     Then the expiry timestamp from "targets.json" has changed
      And the expiry timestamp from "snapshot.json" has changed
      And the expiry timestamp from "timestamp.json" has changed

 Scenario: Refreshing snapshot metadata updates timestamp, but not targets.
    Given I run "sudo cp features/fixtures/config/expiry/short_snapshot_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "targets.json"
      And I record the expiry timestamp from "snapshot.json"
      And I record the expiry timestamp from "timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
     When I run the Rugged command "rugged --debug refresh-expiry"
     Then I should not get:
          """
          Dispatching task to refresh expiring metadata on targets-worker.
          Refreshed targets metadata expiry period.
          Updated snapshot metadata.
          """
      And I should get:
          """
          Dispatching task to refresh expiring metadata on snapshot-worker.
          Refreshed snapshot metadata expiry period.
          Updated timestamp metadata.
          """
     When I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/timestamp.json" to change
     Then the expiry timestamp from "targets.json" has not changed
      And the expiry timestamp from "snapshot.json" has changed
      And the expiry timestamp from "timestamp.json" has changed

 Scenario: Refreshing timestamp metadata updates neither snapshot nor targets.
    Given I run "sudo cp features/fixtures/config/expiry/short_timestamp_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "targets.json"
      And I record the expiry timestamp from "snapshot.json"
      And I record the expiry timestamp from "timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
     When I run the Rugged command "rugged --debug refresh-expiry"
     Then I should not get:
          """
          Refreshed targets metadata expiry period.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
      And I should get:
          """
          Refreshed timestamp metadata expiry period.
          """
     When I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/timestamp.json" to change
     Then the expiry timestamp from "targets.json" has not changed
      And the expiry timestamp from "snapshot.json" has not changed
      And the expiry timestamp from "timestamp.json" has changed

  Scenario: Root metadata expiry is only updated with key rotation.
    Given I run "sudo cp features/fixtures/config/expiry/short_root_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "1.root.json"
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          """
          #This is the short expiry that we set in the config above.
    Given I wait "5" seconds
     When I try to run "sudo sudo -u rugged rugged validate"
     Then I should get:
          """
          error: ExpiredMetadataError thrown in validate_root: 1.root.json is expired
          error: Metadata for the 'root' role is not valid.
          """
      And the expiry timestamp from "1.root.json" has not changed
     When I run the Rugged command "rugged --debug refresh-expiry"
     Then the expiry timestamp from "1.root.json" has not changed
      And the file "2.root.json" does not exist
     When I try to run "sudo sudo -u rugged rugged validate"
     Then I should get:
          """
          error: ExpiredMetadataError thrown in validate_root: 1.root.json is expired
          error: Metadata for the 'root' role is not valid.
          """

  Scenario Outline: Only imminently expiring metadata gets refreshed.
    Given I run "sudo cp features/fixtures/config/expiry/short_<EXPIRING_ROLE>_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "<EXPIRING_ROLE>.json"
      And I record the expiry timestamp from "<STABLE_ROLE1>.json"
      And I record the expiry timestamp from "<STABLE_ROLE2>.json"
     When I run the Rugged command "rugged refresh-expiry"
     Then I should get:
          """
          Refreshed <EXPIRING_ROLE> metadata expiry period.
          """
     Then I should not get:
          """
          Refreshed <STABLE_ROLE1> metadata expiry period.
          Refreshed <STABLE_ROLE2> metadata expiry period.
         """
     Examples:
      | EXPIRING_ROLE | STABLE_ROLE1 | STABLE_ROLE2 |
      | targets       | snapshot     | timestamp    |
      | snapshot      | targets      | timestamp    |
      | timestamp     | targets      | snapshot     |
