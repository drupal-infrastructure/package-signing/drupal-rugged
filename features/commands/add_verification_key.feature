@rugged @command @add-verification-key @local @gitlab-160
Feature: Command to add a verification key to partial root metadata.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to add verification keys to partial root metadata.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged mkdir -p /var/rugged/tuf_repo/partial /var/rugged/tuf_repo/tmp"
    Given the file "/var/rugged/tuf_repo/partial/1.root.json" does not exist
      And the file "/var/rugged/tuf_repo/partial/signable-1.root.json" does not exist
      And I run the Rugged command "rugged initialize-partial-root-metadata"
    Given the file "/var/rugged/tuf_repo/tmp/root.pub" does not exist
      And I run the Rugged command "rugged generate-keys --local --role=root"
      And I run "sudo sudo -u rugged mv /var/rugged/verification_keys/root/root.pub /var/rugged/tuf_repo/tmp/"

  Scenario: The 'add-verification-key' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged add-verification-key --help"
     Then I should not get:
          """
          Error: No such command 'add-verification-key'.
          """
      And I should get:
          """
          Usage: rugged add-verification-key [OPTIONS] ROLE PATH_TO_PUBLIC_KEY

            Add a verification key to partial Root metadata.

          Options:
            --key-type TEXT      The type of key to add ('tuf' or 'pem', default 'tuf').
            --root-expires TEXT  The timestamp that root metadata will expire (eg.
                                 '2123-12-16T16:24:07Z').
            --help               Show this message and exit.
          """

  Scenario: Adding a verification key fails without existing partial root metadata.
    Given I run the Rugged command "rm /var/rugged/tuf_repo/partial/1.root.json"
     When I fail to run the Rugged command "rugged --debug add-verification-key root /var/rugged/tuf_repo/tmp/root.pub"
     Then I should get:
          """
          error: FileNotFoundError thrown in add_verification_key_cmd: Can't open /var/rugged/tuf_repo/partial/1.root.json
          error: A metadata file was not found at the expected path: /var/rugged/tuf_repo/partial/1.root.json
          """

  Scenario: Adding a verification key fails if the role is not configured.
     When I fail to run the Rugged command "rugged --debug add-verification-key intentionally-invalid /var/rugged/tuf_repo/tmp/root.pub"
     Then I should get:
          """
          debug: Checking that 'intentionally-invalid' role is configured in the TUF repo.
          error: The 'intentionally-invalid' role is not configured in the TUF repo.
          """

  Scenario: Adding a verification key fails if the key cannot be read.
     When I fail to run the Rugged command "rugged --debug add-verification-key root /var/rugged/tuf_repo/tmp/intentionally-invalid.pub"
     Then I should get:
          """
          Error: Invalid value for 'PATH_TO_PUBLIC_KEY': Path '/var/rugged/tuf_repo/tmp/intentionally-invalid.pub' does not exist.
          Usage: rugged add-verification-key [OPTIONS] ROLE PATH_TO_PUBLIC_KEY
          Try 'rugged add-verification-key --help' for help.
          """

  Scenario: Add TUF key to root metadata.
    Given I record a reference hash of "/var/rugged/tuf_repo/partial/1.root.json"
    Given I record a reference hash of "/var/rugged/tuf_repo/partial/signable-1.root.json"
     When I run the Rugged command "rugged --debug add-verification-key root /var/rugged/tuf_repo/tmp/root.pub"
     Then I should get:
          """
          debug: Loaded metadata for 'root' role from '/var/rugged/tuf_repo/partial/1.root.json'.
          debug: Wrote 'root' metadata to file '/var/rugged/tuf_repo/partial/1.root.json'.
          """
      And file "/var/rugged/tuf_repo/partial/1.root.json" has changed
      And file "/var/rugged/tuf_repo/partial/signable-1.root.json" has changed
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          root 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids:
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          """
      And I should not get:
          """
          signed by:
          root 1/2
          root 2/2
          snapshot 1/1
          targets 1/1
          timestamp 1/1
          """

  Scenario: Add multiple keys to root metadata.
    Given I run the Rugged command "rugged generate-keys --local --force"
      And I run "sudo sudo -u rugged mv /var/rugged/verification_keys/root/root.pub /var/rugged/tuf_repo/tmp/"
      And I run "sudo sudo -u rugged mv /var/rugged/verification_keys/root/root1.pub /var/rugged/tuf_repo/tmp/"
      And I run "sudo sudo -u rugged mv /var/rugged/verification_keys/snapshot/snapshot.pub /var/rugged/tuf_repo/tmp/"
      And I run "sudo sudo -u rugged mv /var/rugged/verification_keys/targets/targets.pub /var/rugged/tuf_repo/tmp/"
      And I run "sudo sudo -u rugged mv /var/rugged/verification_keys/timestamp/timestamp.pub /var/rugged/tuf_repo/tmp/"
    Given I run the Rugged command "rugged --debug add-verification-key root /var/rugged/tuf_repo/tmp/root.pub"
      And I run the Rugged command "rugged --debug add-verification-key root /var/rugged/tuf_repo/tmp/root1.pub"
      And I run the Rugged command "rugged --debug add-verification-key snapshot /var/rugged/tuf_repo/tmp/snapshot.pub"
      And I run the Rugged command "rugged --debug add-verification-key targets /var/rugged/tuf_repo/tmp/targets.pub"
      And I run the Rugged command "rugged --debug add-verification-key timestamp /var/rugged/tuf_repo/tmp/timestamp.pub"
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          root 1/2:
            type: ed25519
            keyid: 
          root 2/2:
            type: ed25519
            keyid: 
          snapshot 1/1:
            type: ed25519
            keyid: 
          targets 1/1:
            type: ed25519
            keyid: 
          timestamp 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """
      And I should not get:
          """
          signed by:
          """
