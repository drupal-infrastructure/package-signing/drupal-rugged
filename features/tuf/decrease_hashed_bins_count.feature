@rugged @decrease-hashed-bins-count @hashed-bins
Feature: Decrease the number of hashed bins in a running repo.
  In order to optimize performance for client downloads
  As a rugged administrator
  I need to decrease the number of hashed bins in a running repo.

# Note that there are additional tests in `features/commands/update_hashed_bins_count.feature`.
# Those test various safety checks. However, since they're tagged @command,
# they are not run with hashed bins configured.

  Background:
    Given I reset Rugged
      And I run "sudo cp features/fixtures/config/number_of_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged --debug initialize --local"

  Scenario: Create new bins.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation"
     Then I should get:
          """
          Current bin count: 32
          Desired bin count: 16
          Retrieving current bins from repository.
          Retrieved 32 bins, starting with 'bin_00-07' and ending with 'bin_f8-ff'.
          Re-initializing 'bins' and 'bin_n' metadata based on current config.
          Initialized 16 bins, starting with 'bin_0' and ending with 'bin_f'.
          """

  Scenario Outline: Drain the old bins into the new ones.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      And I run the Rugged command "rugged --debug add-targets --local"
     Then I should get:
          """
          debug: Hash of '<filename>': <hash>
          Added target '<filename>' to '<bin_name>' role.
          """
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" contains:
          """
          "<filename>"
          """
      And the following files should not exist:
          """
          /var/rugged/tuf_repo/metadata/<new_bin_name>.json
          """
    Given I am in the "/var/www/html" directory
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     Then I should get:
          """
          debug: Hash of '<filename>': <hash>
          Added target '<filename>' to '<new_bin_name>' role.
          """
      Examples:
       | filename  | contents | new_bin_name | bin_name     | hash |
       | test0.txt | test0    | bin_2        | bin_20-27    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5        | bin_50-57    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |
       | test2.txt | test2    | bin_1        | bin_10-17    | 13e29841e34bc141f8f31eead2f50ce21fd3d09dcf406776d844f6e65b80527c |
       | test3.txt | test3    | bin_3        | bin_30-37    | 36e6fe047847ce478368a46cc151c5762ff461fddd68e2c2bb0250c499be3e97 |
       | test4.txt | test4    | bin_2        | bin_28-2f    | 28abdea4bd1be487ef90bc9fba5667abea260d2009ca8ae5fd788757ded9ce39 |

  Scenario Outline: Clean up old bins.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      And I run the Rugged command "rugged --debug add-targets --local"
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" contains:
          """
          "<filename>"
          """
      And I am in the "/var/www/html" directory
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     Then I should get:
          """
          debug: Deleting '<bin_name>' role from repository.
          Deleted '<bin_name>' role from repository.
          debug: Deleting metadata file: /var/rugged/tuf_repo/metadata/<bin_name>.json
          Deleted metadata file for '<bin_name>' role.
          """
      And the following files should not exist:
          """
          /var/rugged/tuf_repo/metadata/<bin_name>.json
          """
      Examples:
       | filename  | contents | new_bin_name | bin_name     | hash |
       | test0.txt | test0    | bin_2        | bin_20-27    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5        | bin_50-57    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |
       | test2.txt | test2    | bin_1        | bin_10-17    | 13e29841e34bc141f8f31eead2f50ce21fd3d09dcf406776d844f6e65b80527c |
       | test3.txt | test3    | bin_3        | bin_30-37    | 36e6fe047847ce478368a46cc151c5762ff461fddd68e2c2bb0250c499be3e97 |
       | test4.txt | test4    | bin_2        | bin_28-2f    | 28abdea4bd1be487ef90bc9fba5667abea260d2009ca8ae5fd788757ded9ce39 |

  Scenario Outline: Write new bins to disk.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      And I run the Rugged command "rugged add-targets --local"
      And I am in the "/var/www/html" directory
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     Then I should get:
          """
          debug: Hash of '<filename>': <hash>
          Added target '<filename>' to '<new_bin_name>' role.
          debug: Wrote '<new_bin_name>' metadata to file '/var/rugged/tuf_repo/metadata/<new_bin_name>.json'.
          """
      And the file "/var/rugged/tuf_repo/metadata/<new_bin_name>.json" contains:
          """
          "<filename>"
          """
      And the following files should exist:
          """
          /var/rugged/tuf_repo/metadata/bin_0.json
          /var/rugged/tuf_repo/metadata/bin_f.json
          """
      Examples:
       | filename  | contents | new_bin_name | bin_name     | hash |
       | test0.txt | test0    | bin_2        | bin_20-27    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5        | bin_50-57    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |
       | test2.txt | test2    | bin_1        | bin_10-17    | 13e29841e34bc141f8f31eead2f50ce21fd3d09dcf406776d844f6e65b80527c |
       | test3.txt | test3    | bin_3        | bin_30-37    | 36e6fe047847ce478368a46cc151c5762ff461fddd68e2c2bb0250c499be3e97 |
       | test4.txt | test4    | bin_2        | bin_28-2f    | 28abdea4bd1be487ef90bc9fba5667abea260d2009ca8ae5fd788757ded9ce39 |

  Scenario Outline: Trigger updates to all and only relevant metadata.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      And I run the Rugged command "rugged add-targets --local"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/targets.json"
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/bins.json"
      And I am in the "/var/www/html" directory
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     Then the file "/var/rugged/tuf_repo/metadata/<new_bin_name>.json" contains:
          """
          "<filename>"
          """
      And file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
      And file "/var/rugged/tuf_repo/metadata/timestamp.json" has changed
      And file "/var/rugged/tuf_repo/metadata/snapshot.json" has changed
      And file "/var/rugged/tuf_repo/metadata/targets.json" has not changed
      And file "/var/rugged/tuf_repo/metadata/bins.json" has changed
      Examples:
       | filename  | contents | new_bin_name | bin_name     | hash |
       | test0.txt | test0    | bin_2        | bin_20-27    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5        | bin_50-57    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |

  Scenario Outline: Check that the repository remains valid/consistent.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      And I run the Rugged command "rugged add-targets --local"
      And I am in the "/var/www/html" directory
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          Metadata for the 'bins' role is valid.
          Metadata for the 'bin_0' role is valid.
          Metadata for the '<new_bin_name>' role is valid.
          Metadata for the 'bin_f' role is valid.
          """
      Examples:
       | filename  | contents | new_bin_name | bin_name     | hash |
       | test0.txt | test0    | bin_2        | bin_20-27    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5        | bin_50-57    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |

  Scenario Outline: Check that the repository remains valid/consistent when signing on workers.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      # This has to remain local, sine the targets-worker is configured to use 16 bins.
      And I run the Rugged command "rugged add-targets --local"
      And I am in the "/var/www/html" directory
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation"
      And I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          Metadata for the 'bins' role is valid.
          Metadata for the 'bin_0' role is valid.
          Metadata for the '<new_bin_name>' role is valid.
          Metadata for the 'bin_f' role is valid.
          """
      Examples:
       | filename  | contents | new_bin_name | bin_name     | hash |
       | test0.txt | test0    | bin_2        | bin_20-27    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5        | bin_50-57    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |
