@rugged @openssl @gitlab-160
Feature: Ensure that we have a modern OpenSSL.
  In order to test HSM-based root keys
  As an developer
  I need to emulate HSM opertions using OpenSSL.

  Background:
    Given I run "make clean-fixtures"
      And I run "mkdir -p fixtures/external_keys fixtures/tmp"
      And I am in the "fixtures/external_keys" directory

  Scenario: Ensure that we have a modern version of OpenSSL available.
     When I run "/usr/local/ssl/bin/openssl version"
     Then I should get:
          """
          OpenSSL 3.0.12 24 Oct 2023 (Library: OpenSSL 3.0.12 24 Oct 2023)
          """

  Scenario: Ensure that the version of OpenSSL in Python is modern enough.
     When I run "python -c 'import cryptography.hazmat.backends.openssl as openssl; print(openssl.backend.openssl_version_text())'"
     Then I should get:
          """
          OpenSSL 3
          """

  Scenario: Ensure that we can use OpenSSL to generate keys that can emulate an HSM.
          # Generate a PEM-encoded ed25519 private key for the Root role.
          # This is intended to emulate a key generated on a YubiHSM.
          # It will be used later for signing.
          # See: https://cendyne.dev/posts/2022-03-06-ed25519-signatures.html#generating-our-own-ed25519-key
     When I run "/usr/local/ssl/bin/openssl genpkey -algorithm ED25519 -out private.pem"
      And I run "/usr/local/ssl/bin/openssl pkey -in private.pem -text"
     Then I should get:
          """
          -----BEGIN PRIVATE KEY-----
          -----END PRIVATE KEY-----
          ED25519 Private-Key:
          priv:
          pub:
          """
          # Extract a PEM-encoded ed25519 public key for the Root role.
          # This is intended to emulate a public key exported by a YubiHSM.
          # See: https://docs.yubico.com/hardware/yubihsm-2/hsm-2-user-guide/hsm2-cmd-reference.html#get-public-key-command
     When I run "/usr/local/ssl/bin/openssl pkey -in private.pem -pubout > public.pem"
      And I run "/usr/local/ssl/bin/openssl pkey -in public.pem -pubin -text"
     Then I should get:
          """
          -----BEGIN PUBLIC KEY-----
          -----END PUBLIC KEY-----
          ED25519 Public-Key:
          pub:
          """
      And I should not get:
          """
          -----BEGIN PRIVATE KEY-----
          -----END PRIVATE KEY-----
          ED25519 Private-Key:
          priv:
          """

  Scenario: Ensure that we can use OpenSSL to generate signatures that can emulate an HSM.
          # Generate a PEM-encoded ed25519 keypair to emulate an HSM. See above.
    Given I run "/usr/local/ssl/bin/openssl genpkey -algorithm ED25519 -out private.pem"
      And I run "/usr/local/ssl/bin/openssl pkey -in private.pem -pubout > public.pem"
          # Create a test file to sign.
      And I run "echo 'Hello World!' > ../tmp/input.txt"
          # Generate a signature of the test file.
          # This should be the equivalent of signing with the HSM.
          # See: https://docs.yubico.com/hardware/yubihsm-2/hsm-2-user-guide/hsm2-cmd-reference.html#sign-eddsa-command
     When I run "/usr/local/ssl/bin/openssl pkeyutl -in ../tmp/input.txt -rawin -sign -inkey private.pem > signature.bin"
          # Verify the signature of the test file using the public key.
      And I run "/usr/local/ssl/bin/openssl pkeyutl -verify -sigfile signature.bin -in ../tmp/input.txt -rawin -inkey public.pem -pubin"
     Then I should get:
          """
          Signature Verified Successfully
          """
