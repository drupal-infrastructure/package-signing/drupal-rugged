include .mk/GNUmakefile
.DEFAULT_GOAL := help-rugged

enable-hashed-bins:
	@ddev targets-worker-enable-hashed-bins
	@ddev snapshot-worker-enable-hashed-bins
	@ddev root-worker-enable-hashed-bins

disable-hashed-bins:
	@ddev targets-worker-disable-hashed-bins
	@ddev snapshot-worker-disable-hashed-bins
	@ddev root-worker-disable-hashed-bins
