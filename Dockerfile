FROM debian:11-slim

USER root

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -yqq update
RUN apt-get -y -qq install \
    apt-utils \
    apt-transport-https \
    ca-certificates


WORKDIR /opt/rugged

# Get rugged source
COPY rugged rugged
COPY Pipfile Pipfile.lock setup.py ./

# Copy all worker scripts, image may run as any worker type.
COPY rugged/workers /usr/local/bin/

# Disable warning message about running pip as root.
ENV PIP_ROOT_USER_ACTION=ignore
ENV LANG C.UTF-8

# runtime dependencies:
RUN apt-get install -yqq python3-minimal \
    openssh-client # (required for keygen?)
    #libpcre3-dev  (Was dependency in ansible, I am doubtful this is runtime dep...)

# Install rugged, generate requirements.txt from Pipfile.lock, as pipenv 2023.7 no longer
# allows adding rugged to Pipfile and updating lock with minimal changes, inadvertantly updating
# dependency versions specified in lock. Using pip install with generated requirements  pins
# versions and installs rugged to system.
# Cleanup and uninstall pip, pipenv, venvs, cache, etc.
RUN apt-get install -yqq python3-pip && \
    pip3 install --no-cache-dir --upgrade pip && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3 1 && \
    pip3 install --no-cache-dir -q pipenv --upgrade && \
    pipenv requirements > requirements.txt && \
    pip install -r requirements.txt . && \
    pip3 uninstall -y -q pipenv && \
    apt-get --purge -yqq autoremove python3-pip && \
    apt-get clean && \
    rm -rf /root/.local/share/virtualenvs/ && \
    rm -rf /root/.cache

# Add rugged user
RUN groupadd -g 440 rugged
RUN useradd -u 440 -g 440 -ms /bin/bash rugged

# Rugged is installed to system, remove source, preserve default_config.yaml
RUN mv ./rugged/default_config.yaml /tmp/default_config.yaml && \
    rm -rf /opt/rugged && \
    mkdir -p /opt/rugged/rugged && \
    mv /tmp/default_config.yaml /opt/rugged/rugged

# Rugged log dir
RUN mkdir /var/log/rugged && \
    chmod 2755 /var/log/rugged && \
    chown rugged:rugged /var/log/rugged

# Rugged log file
RUN touch /var/log/rugged/rugged.log  && \
    chmod 0755 /var/log/rugged/rugged.log && \
    chown rugged:rugged /var/log/rugged/rugged.log

# Rugged source dir
RUN chmod 2755 /opt/rugged && \
    chown -R rugged:rugged /opt/rugged

# Rugged pid dir
RUN mkdir -p /var/run/rugged && \
    chmod 2755 /var/run/rugged && \
    chown rugged:rugged /var/run/rugged

# This will change worker type in startup script,
# should be set during deployment.
ENV RUGGED_WORKER=unnamed-worker

COPY ./start_celery.sh /
USER rugged

# for non-root container...
ENTRYPOINT ["/start_celery.sh"]
