import os
from glob import glob
from re import sub
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.lib.config import get_config
from rugged.lib.constants import RUGGED_PARTIAL_METADATA_DIR
from rugged.lib.logger import get_logger, log_exception
from securesystemslib.exceptions import StorageError
from tuf.api.metadata import Metadata
from tuf.api.serialization.json import DeserializationError
from typing import Optional

config = get_config()
log = get_logger()

#@TODO: add version lookup to add-verification-key and add-root-signature as well
#@TODO: use the same mechanism to look up partial metadata in remove-verification-key
#@TODO: use the same mechanism for show-partial-metadata (and factor out version parameter)

def get_current_partial_root_metadata_path():
    """ Look up the full path for the current partial root metadata file. """
    metadata_dir = RUGGED_PARTIAL_METADATA_DIR
    return get_metadata_path('root', metadata_dir)

def load_production_metadata_from_file(role_name: str):
    """ Load current version of production Metadata. """
    metadata_dir = config['repo_metadata_path'].get()
    return _load_metadata_from_file(role_name, metadata_dir)

def load_partial_metadata_from_file(role_name: str):
    """ Load current partial Metadata. """
    metadata_dir = RUGGED_PARTIAL_METADATA_DIR
    return _load_metadata_from_file(role_name, metadata_dir)

def _load_metadata_from_file(role_name: str, metadata_dir: str):
    """ Load a role's metadata from storage. """

    try:
        path = get_metadata_path(role_name, metadata_dir)
        log.debug(f"Loading metadata for '{role_name}' from {path}.")

        metadata = Metadata
        loaded_metadata = metadata.from_file(path)
        log.debug(f"Loaded metadata for '{role_name}' role from '{path}'.")
        return loaded_metadata
    except TypeError as e:
        log_exception(e)
        error = f"Failed to load metadata for '{role_name}' role from '{path}'."
        raise RuggedMetadataError(error)
    except DeserializationError as e:
        log_exception(e)
        error = f"Failed to deserialize data from '{path}'."
        raise RuggedMetadataError(error)
    except (FileNotFoundError, StorageError) as e:
        raise FileNotFoundError(e)

# @TODO: Deduplicate the private versions of this method in the repo and validator modules.
def get_metadata_path(role_name, metadata_dir: str):
    """ Determine the path for a given role's metadata file. """
    filename = _get_metadata_filename(role_name, metadata_dir)
    return os.path.join(metadata_dir, filename)

def _get_metadata_filename(role_name, metadata_dir):
    """ Determine the filename for a given role's metadata. """
    filename = f"{role_name}.json"
    # Timestamp metadata never uses consistent snapshots.
    if role_name == "timestamp":
        return filename
    # We need to special-case root here, since it should always have the
    # version prefix. This is what will allow clients to find and validate
    # the chain of root metadata that links the original (shipped) root to
    # its current version.
    if role_name == 'root' or config['consistent_snapshot'].get():
        try:
            # Find the most recent one that exists in storage (eg. on disk).
            filename = _get_latest_metadata_filename(filename, metadata_dir)
        except FileNotFoundError as e:
            # If no versioned filename exists, initialize version 1.
            filename = f"1.{filename}"

    return filename

def _get_latest_metadata_filename(filename, metadata_dir):
    """ Determine the latest filename for a given role's metadata. """
    os.chdir(metadata_dir)
    files = glob(f"[0-9].{filename}")
    # Sort numbers naturally. Based on magic from https://stackoverflow.com/a/33159707.
    files.sort(key=lambda f: int(sub(r'\D', '', f)))
    if not files:
        raise FileNotFoundError
    # Return the latest version.
    return files[-1]
