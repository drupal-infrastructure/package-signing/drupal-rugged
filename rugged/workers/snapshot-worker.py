from rugged.lib.expiry import (
    get_expiring_metadata,
    refresh_expiry,
)
from rugged.lib.logger import (
    get_rugged_logger,
    set_log_level_from_context,
)
from rugged.lib.task_queue import TaskQueue
from rugged.tuf.repo import RuggedRepository
from rugged.workers.base_worker import BaseWorker

log = get_rugged_logger()
queue = 'snapshot-worker'
worker = TaskQueue().get_task_queue()


class SnapshotWorker(BaseWorker):
    """ Rugged (Celery) worker that fulfills the TUF 'snapshot' role. """

    @worker.task(name='update_snapshot_task', queue=queue)
    def update_snapshot_task(**context):
        """ Task to update snapshot metadata for a TUF repository. """
        set_log_level_from_context(context)
        log.info("Received update-snapshot task.")
        repo = RuggedRepository()
        repo.load()
        repo.update_snapshot()
        result = repo.write_metadata('snapshot')
        if result:
            message = "Updated snapshot metadata."
            log.info(message)
        else:
            message = "Failed to refresh snapshot metadata."
            log.error(message)
        return (result, message)

    @worker.task(name='get_expiring_metadata_task', queue=queue)
    def get_expiring_metadata_task(**context):
        """ Task to return a list of imminently expiring metadata. """
        set_log_level_from_context(context)
        log.info("Received get-expiring-metadata task.")
        return get_expiring_metadata('snapshot')

    @worker.task(name='refresh_expiry_task', queue=queue)
    def refresh_expiry_task(**context):
        """ Task to refresh snapshot metadata expiry period. """
        set_log_level_from_context(context)
        log.info("Received refresh-expiry task.")
        return refresh_expiry('snapshot')
