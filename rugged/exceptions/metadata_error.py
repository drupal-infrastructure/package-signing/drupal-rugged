from rugged.lib.logger import get_logger
from tuf.api.exceptions import UnsignedMetadataError

log = get_logger()


class RuggedMetadataError(UnsignedMetadataError):

    def __init__(self, msg="Error generating TUF metadata."):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
