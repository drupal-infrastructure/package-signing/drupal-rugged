from rugged.lib.logger import get_logger

log = get_logger()


class RuggedKeyError(AttributeError):

    def __init__(self, msg="Error loading TUF keys."):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
