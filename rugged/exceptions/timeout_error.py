from celery.exceptions import TimeoutError
from rugged.lib.logger import get_logger

log = get_logger()


class RuggedTimeoutError(TimeoutError):

    def __init__(self, msg="The operation exceeded the given deadline."):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
