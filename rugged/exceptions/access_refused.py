from amqp.exceptions import AccessRefused
from rugged.lib.logger import get_logger

log = get_logger()


class RuggedAccessRefused(AccessRefused):

    def __init__(self, msg="Failed to authenticate to RabbitMQ."):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
