from rugged.lib.logger import get_logger
from tuf.api.exceptions import RepositoryError

log = get_logger()


class RuggedRepositoryError(RepositoryError):

    def __init__(
        self,
        msg="The repository could not load a repository at the given path."
    ):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
