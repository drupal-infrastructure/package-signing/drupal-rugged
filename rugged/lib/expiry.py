from datetime import datetime, timedelta
from rugged.lib.config import get_config
from rugged.lib.logger import (
    get_rugged_logger,
    log_exception,
)
from rugged.tuf.repo import RuggedRepository

config = get_config()
log = get_rugged_logger()


def expiry_is_imminent(current_expiry_timestamp: datetime):
    """ Determine whether a given expiry datetime will expire imminently. """
    log.debug(f"Time when metadata expires: {current_expiry_timestamp}")
    expiry_refresh_threshold = config['expiry_refresh_threshold'].get()
    log.debug(f"Expiry refresh threshold is {expiry_refresh_threshold} seconds.")
    expiry_imminent_timestamp = datetime.utcnow().replace(microsecond=0) + timedelta(seconds=expiry_refresh_threshold)
    log.debug(f"Time when refresh threshold is reached: {expiry_imminent_timestamp}")
    return current_expiry_timestamp < expiry_imminent_timestamp


def get_expiring_metadata(role):
    """ Return a list of metadata that will expire imminently. """
    try:
        expiring_metadata = []
        repo = RuggedRepository()
        repo.load()
        if expiry_is_imminent(repo.roles[role].signed.expires):
            log.debug(f"'{role}.json' metadata expiry is imminent.")
            expiring_metadata.append(role)
        return (True, expiring_metadata)
    except Exception as e:
        log_exception(e)
        message = f"Failure during attempt to get {role} metadata that will expire soon."
        log.error(message)
        return (False, expiring_metadata)


def refresh_expiry(role):
    """ Refresh metadata expiry period. """
    try:
        repo = RuggedRepository()
        repo.load()
        repo.update_metadata_expiry(role)
        repo.roles[role].signatures.clear()
        repo.sign_metadata(role)
        if repo.write_metadata(role):
            message = f"Refreshed {role} metadata expiry period."
            log.info(message)
            return (True, message)
        else:
            message = f"Failed to refresh {role} metadata expiry period."
            log.error(message)
            return (False, message)
    except Exception as e:
        log_exception(e)
        message = f"Failure during attempt to refresh {role} expiry period."
        log.error(message)
        return (False, message)
