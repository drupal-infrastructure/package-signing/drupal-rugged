__version__ = "0.1.0-dev"
RUGGED_LOGGER = 'rugged'

RUGGED_SIGNING_KEY_DIR = '/var/rugged/signing_keys'
RUGGED_VERIFICATION_KEY_DIR = '/var/rugged/verification_keys'
RUGGED_PARTIAL_METADATA_DIR = '/var/rugged/tuf_repo/partial'

RUGGED_KEY_INDEX_DELIMITER = ':'

RUGGED_MONITOR_TUF_PROCESSING_PREFIX = 'tuf_processing_'
RUGGED_MONITOR_TUF_READY_PREFIX = 'tuf_ready_'
RUGGED_MONITOR_TUF_REFRESHING_FLAG = 'tuf_refreshing_expiry'
RUGGED_MONITOR_TUF_PAUSED_FLAG = 'tuf_paused'

RUGGED_TUF_WORKERS = ['targets-worker', 'snapshot-worker', 'timestamp-worker']
RUGGED_MONITOR_WORKER = 'monitor-worker'

RUGGED_OUTPUT_COLOUR_RESET = "\x1b[0m"
RUGGED_OUTPUT_COLOUR_GREEN = "\x1b[38;5;82m"
RUGGED_OUTPUT_COLOUR_YELLOW = "\x1b[38;5;184m"
