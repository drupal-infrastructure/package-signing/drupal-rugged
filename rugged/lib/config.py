import confuse
from os.path import expanduser, isfile


class Config:

    def __init__(self):
        self.config = confuse.Configuration('rugged', 'rugged')
        self.load_default_configuration()
        self.load_system_configuration()
        self.load_user_configuration()

    def get_config(self):
        """ Return the config object """
        return self.config

    def load_default_configuration(self):
        """ Load package default configuration. """
        config_file = '/opt/rugged/rugged/default_config.yaml'
        self.load_configuration(config_file)

    def load_system_configuration(self):
        """ Load system-side configuration. """
        config_file = '/etc/rugged/config.yaml'
        self.load_configuration(config_file)

    def load_user_configuration(self):
        """ Load user-specific configuration. """
        home = expanduser("~")
        config_file = f'{home}/.config/rugged/config.yaml'
        self.load_configuration(config_file)

    def load_configuration(self, config_file):
        """ Load a given config file, if it exists. """
        if isfile(config_file):
            self.config.set_file(config_file)

def get_local_configs():
    """ Return a consolidated/realized config dict. """
    config = get_config()
    config['broker_connection_string'].redact = True
    return config.flatten(redact=True)

def get_config():
    """ Return the (Confuse) config object. """
    return Config().get_config()
