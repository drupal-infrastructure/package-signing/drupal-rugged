import click
import click_log
import logging
from rugged.commands.add_targets import add_targets_cmd
from rugged.commands.add_root_signature import add_root_signature_cmd
from rugged.commands.add_verification_key import add_verification_key_cmd
from rugged.commands.check_monitor import check_monitor_cmd
from rugged.commands.config import config_cmd
from rugged.commands.echo import echo_cmd
from rugged.commands.generate_keys import generate_keys_cmd
from rugged.commands.initialize import initialize_cmd
from rugged.commands.initialize_partial_root_metadata import initialize_partial_root_metadata_cmd
from rugged.commands.initialize_partial_root_metadata_for_update import initialize_partial_root_metadata_for_update_cmd
from rugged.commands.logs import logs_cmd
from rugged.commands.pause_processing import pause_processing_cmd
from rugged.commands.refresh_expiry import refresh_expiry_cmd
from rugged.commands.remove_targets import remove_targets_cmd
from rugged.commands.remove_verification_key import remove_verification_key_cmd
from rugged.commands.resume_processing import resume_processing_cmd
from rugged.commands.reset_processing import reset_processing_cmd
from rugged.commands.reset_refreshing import reset_refreshing_cmd
from rugged.commands.rotate_keys import rotate_keys_cmd
from rugged.commands.show_partial_root_metadata import show_partial_root_metadata_cmd
from rugged.commands.sleep import sleep_cmd
from rugged.commands.status import status_cmd
from rugged.commands.update_hashed_bins_count import update_hashed_bins_count_cmd
from rugged.commands.validate import validate_cmd
from rugged.lib.constants import __version__
from rugged.lib.logger import get_rugged_logger
from rugged.lib.user import check_user

check_user()
log = get_rugged_logger()


@click.group()
@click.version_option(version=__version__)
@click_log.simple_verbosity_option(log)
@click.option('--debug', is_flag=True, help='Output and log debug messages.')
def rugged_cli(debug) -> None:
    """A CLI tool for the Rugged TUF Server."""
    if debug:
        log.setLevel(logging.DEBUG)
    log.debug("rugged_cli invoked")


def register_subcommands() -> None:
    """Add sub-commands to the CLI tool."""
    rugged_cli.add_command(add_targets_cmd)
    rugged_cli.add_command(add_root_signature_cmd)
    rugged_cli.add_command(add_verification_key_cmd)
    rugged_cli.add_command(check_monitor_cmd)
    rugged_cli.add_command(config_cmd)
    rugged_cli.add_command(echo_cmd)
    rugged_cli.add_command(generate_keys_cmd)
    rugged_cli.add_command(initialize_cmd)
    rugged_cli.add_command(initialize_partial_root_metadata_cmd)
    rugged_cli.add_command(initialize_partial_root_metadata_for_update_cmd)
    rugged_cli.add_command(logs_cmd)
    rugged_cli.add_command(pause_processing_cmd)
    rugged_cli.add_command(refresh_expiry_cmd)
    rugged_cli.add_command(remove_targets_cmd)
    rugged_cli.add_command(remove_verification_key_cmd)
    rugged_cli.add_command(resume_processing_cmd)
    rugged_cli.add_command(reset_processing_cmd)
    rugged_cli.add_command(reset_refreshing_cmd)
    rugged_cli.add_command(rotate_keys_cmd)
    rugged_cli.add_command(show_partial_root_metadata_cmd)
    rugged_cli.add_command(sleep_cmd)
    rugged_cli.add_command(status_cmd)
    rugged_cli.add_command(update_hashed_bins_count_cmd)
    rugged_cli.add_command(validate_cmd)


register_subcommands()
