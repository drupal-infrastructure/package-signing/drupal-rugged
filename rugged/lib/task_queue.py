import os
import sys
from amqp.exceptions import AccessRefused
from celery import Celery
from celery.exceptions import TimeoutError
from kombu.exceptions import OperationalError
from rugged.lib.config import get_config
from rugged.exceptions.access_refused import RuggedAccessRefused
from rugged.exceptions.connection_error import RuggedConnectionError
from rugged.exceptions.timeout_error import RuggedTimeoutError
from rugged.lib.logger import (
    get_logger,
    log_exception,
)

log = get_logger()
config = get_config()
task_timeout = config['task_timeout'].get()


class TaskQueue:
    """ Return a fully initialized task queue. """

    def __init__(self, broker_connection_string = None):
        self.log = log
        self.config = config
        if broker_connection_string is None:
            broker_connection_string = self.config['broker_connection_string'].get()
        try:
            self.log.debug("Initializing connection to RabbitMQ.")
            self.celery = Celery(
                '',
                broker=broker_connection_string,
                backend='rpc://',
            )
        except AccessRefused as e:
            log_exception(e)
            self.log.error("Failed to authenticate to RabbitMQ.")
            raise RuggedAccessRefused

        worker_max_memory_per_child = self.config['celery_worker_max_memory_per_child'].get()

        self.celery.conf.update(
            task_serializer='json',
            result_persistent = True,
            result_serializer='json',
            broker_transport_options={'max_retries': 1},
            worker_max_memory_per_child = worker_max_memory_per_child
        )

    def get_task_queue(self):
        return self.celery

    def send_task(self, task=None, worker=None, args=[], timeout=task_timeout):
        # Pass context as keyword arguments.
        context = {
            "log_level": self.log.level
        }
        try:
            queue = self.get_task_queue()
            result = queue.send_task(
                task,
                queue=worker,
                args=args,
                kwargs=context,
            )
            # @TODO: Evaluate if there's a better way to handle sub-tasks.
            # See: https://docs.celeryq.dev/en/latest/userguide/tasks.html#avoid-launching-synchronous-subtasks
            return result.get(timeout=timeout, disable_sync_subtasks=False)
        except TimeoutError as e:
            log_exception(e)
            raise RuggedTimeoutError
        except AccessRefused as e:
            log_exception(e)
            raise RuggedAccessRefused
        except OperationalError as e:
            log_exception(e)
            raise RuggedConnectionError


def run_task(
    worker,
    task,
    args=[],
    timeout=task_timeout,
    broker_connection_string=None,
):
    """ Run a task on a worker and return the response. """
    try:
        queue = TaskQueue(broker_connection_string)
        return queue.send_task(
            task=f"{task}",
            worker=worker,
            args=args,
            timeout=timeout,
        )
    except RuggedTimeoutError:
        warning = "The operation timed out. "\
                f"Check status of {worker}."
        log.warning(warning)
        raise RuggedTimeoutError(warning)
    except RuggedConnectionError:
        error = "Failed to connect to worker queue. "\
                "Check configuration."
        log.error(error)
        sys.exit(os.EX_NOHOST)
    except RuggedAccessRefused:
        error = "Failed to authenticate to worker queue. "\
                "Check credentials."
        log.error(error)
        sys.exit(os.EX_NOPERM)
