import click
from rugged.commands.lib.expiry import (
    fetch_all_targets_metadata,
    fetch_expiring_metadata,
    refresh_metadata,
)
from rugged.lib.logger import get_logger

log = get_logger()


@click.command("refresh-expiry")
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Force refresh of all metadata expiry periods.",
)
def refresh_expiry_cmd(force) -> None:
    """ Refresh expiry period for any metadata where it will expire imminently. """

    log.info("Preparing to refresh metadata expiry periods.")

    if force:
        # If any targets metadata requires a refresh, snapshot and timestamp
        # will have to be updated. So we only need to force fetching all
        # targets metadata.
        metadata = fetch_all_targets_metadata()
        log.info("Forcing refresh of all metadata.")
    else:
        metadata = fetch_expiring_metadata()
        for tuf_worker, role_list in metadata.items():
            log.info(f"Expiring metadata from {tuf_worker}:")
            if not role_list:
                log.info("  No expiring metadata.")
            for role in role_list:
                log.info(f"  {role}.json")

    refresh_metadata(metadata)
