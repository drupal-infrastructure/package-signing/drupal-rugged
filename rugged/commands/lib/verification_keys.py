import sys
from binascii import hexlify
from cryptography.hazmat.primitives.serialization import (
    Encoding,
    PublicFormat,
    load_pem_public_key,
)
from rugged.lib.logger import get_logger
from securesystemslib.formats import encode_canonical
from securesystemslib.hash import digest
from securesystemslib.interface import import_ed25519_publickey_from_file
from securesystemslib.keys import format_keyval_to_metadata
from typing import Any, Dict, Tuple

log = get_logger()


def load_verification_key(key_type: str, path_to_public_key: str) -> Tuple[str, Dict[str, Any]]:
    """ Load a key from disk and return it as a TUF key. """

    # @TODO: Use match statement once we move to Python 3.10.
    # @TODO: Also, move to Python 3.10.
    if key_type == 'tuf':
        return _load_tuf_verification_key(path_to_public_key)
    elif key_type == 'pem':
        return _load_pem_verification_key(path_to_public_key)
    else:
        log.error(f"The '{key_type}' key type is not supported.")
        sys.exit(os.EX_USAGE)


def _load_tuf_verification_key(path_to_public_key: str) -> Tuple[str, Dict[str, Any]]:
    """ Load a TUF-formatted public key from disk. """

    log.debug(f"Loading TUF public key from: {path_to_public_key}")
    key = import_ed25519_publickey_from_file(path_to_public_key)
    log.debug(f"Loaded TUF public key from: {path_to_public_key}")

    keyid = key.pop('keyid')
    del(key['keyid_hash_algorithms'])
    return (keyid,key)


def _load_pem_verification_key(path_to_public_key: str) -> Tuple[str, Dict[str, Any]]:
    """ Load a PEM-encoded public key from disk. """

    with open(path_to_public_key, "rb") as file:
        log.debug(f"Loading PEM-encoded public key from: {path_to_public_key}")
        public_key = load_pem_public_key(file.read())
        log.debug(f"Loaded PEM-encoded public key from: {path_to_public_key}")

    key_string = hexlify(public_key.public_bytes(Encoding.Raw, PublicFormat.Raw)).decode()
    key_value = { "public": key_string }
    keyid = get_keyid(key_value)
    key = {
        "keytype": _get_key_type(),
        "scheme": _get_key_scheme(),
        "keyval": key_value,
    }
    return (keyid,key)


def get_keyid(key_value):
    """ Return the keyid of a given TUF key dict. """

    # See: securesystemslib.keys._get_keyid()
    key_meta = format_keyval_to_metadata(_get_key_type(), _get_key_scheme(), key_value)
    key_update_data = encode_canonical(key_meta)

    digest_object = digest("sha256")
    digest_object.update(key_update_data.encode("utf-8"))
    keyid = digest_object.hexdigest()

    return keyid


def _get_key_type() -> str:
    """ Return the key type. """

    return 'ed25519'


def _get_key_scheme() -> str:
    """ Return the key scheme. """

    return 'ed25519'
