# Rugged - A TUF server

[![Pipeline status](https://gitlab.com/rugged/rugged/badges/main/pipeline.svg)](https://gitlab.com/rugged/rugged/-/commits/main)

Rugged is an opinionated implementation of [The Update Framework (TUF)](https://theupdateframework.io/). Originally developed for and sponsored by the Drupal Association.

For details, please refer to the [documentation site](https://rugged.works/)

## Security reporting

Please report security issues to security@rugged.works. See [SECURITY.md](https://rugged.works/SECURITY.md) for details.
